package dnm.com.rmz.Activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;

import dnm.com.rmz.Bean.Birthday;
import dnm.com.rmz.Bean.Gallery;
import dnm.com.rmz.Common.ExceptionHandler;
import dnm.com.rmz.Common.SessionManager;
import dnm.com.rmz.R;
import dnm.com.rmz.Service.TVService;
import dnm.com.rmz.utilities.CustomImageView;


public class BdayActivity extends Activity {

    CustomImageView imagebackground,imageframe;
    TextView tvpersonname;
    List<Birthday> birthdayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.fragment_happie_bady);

        imageframe = (CustomImageView) findViewById(R.id.f1_bday_frame_image);
        tvpersonname= (TextView) findViewById(R.id.f1_person_name);
        imagebackground= (CustomImageView) findViewById(R.id.f1_bg_template);

        imagebackground.setImageUrl("http://thebay.dnmdigital.com/androidtv/uploads/images/birthdaytemplate/birthday.jpg");
        birthdayList= (ArrayList<Birthday>) SessionManager.RetrieveData("bdaylist");

        stopService(new Intent(BdayActivity.this, TVService.class));

        try
        {

        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {

            int i = 0;

            public void run() {

                if (i< birthdayList.size())
                {
                    tvpersonname.setText(birthdayList.get(i).getName());
                    imageframe.setImageUrl(birthdayList.get(i).getImage());
                }
                if (i< birthdayList.size()) {
                    handler.postDelayed(this, birthdayList.get(i).getDuration() * 1000);
                    i++;
                }
                if (birthdayList.size() == i){

                    startService(new Intent(getApplicationContext(), TVService.class));
                    BdayActivity.this.finish();

                }

                if (i > birthdayList.size() - 1) {
                    Log.e("image",""+birthdayList.size()+" "+i);

                }

            }
        };
        handler.postDelayed(runnable, 0);

        }catch (Exception e)
        {
            Intent intent =new Intent(getApplicationContext(),SplashScreenActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
            finish();
            e.printStackTrace();
        }

    }

}
