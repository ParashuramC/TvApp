package dnm.com.rmz.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import dnm.com.rmz.Bean.Birthday;
import dnm.com.rmz.Bean.Gallery;
import dnm.com.rmz.Bean.News_tech;
import dnm.com.rmz.Bean.HrAnnoucement;
import dnm.com.rmz.Bean.Quotes;
import dnm.com.rmz.Bean.Video;
import dnm.com.rmz.Bean.Wheather;
import dnm.com.rmz.Common.CommonJsonParsing;
import dnm.com.rmz.Common.ExceptionHandler;
import dnm.com.rmz.Common.SessionManager;
import dnm.com.rmz.Database.Rmz_Db;
import dnm.com.rmz.R;
import dnm.com.rmz.Service.TVService;
import dnm.com.rmz.utilities.CustomImageView;
import dnm.com.rmz.utilities.Utils;


public class GalleryActivity extends Activity {

    CustomImageView imagedefault;
    Context context;
    ArrayList<Gallery> tvdefaultlist;
    public static final String PREFS_NAME = "MyApp_Settings";
    ArrayList<Integer> array_image;

    TimerTask timer;
    ImageView imagesplash;
    CommonJsonParsing commonJsonParsing;
    private String result;
    List<News_tech> news_list;
    List<Video> videoList;
    ArrayList<Gallery> gallery_list;
    ArrayList final_gallery_list;
    List<Wheather> final_wheather_list;
    List<Birthday> birthday_list;
    public static String BASE_URL = "http://thebay.in/androidtv/application/views/admin/register.php";
    //  public static String BASE_URL = "http://dnmdigital.com/projects/androidtv/application/views/admin/register.php";
    private static String TAG = "SplashScreenActivity";
    JSONObject object;
    JSONArray parentarray;
    private JSONArray parentarray1;

    List<Video> list_videos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
       Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_default);

        context=this;
        imagedefault = (CustomImageView) findViewById(R.id.default_images);

        list_videos=new ArrayList<>();
        array_image= (ArrayList<Integer>) SessionManager.RetrieveData("Image_set");
        tvdefaultlist= (ArrayList<Gallery>) SessionManager.RetrieveData("galleryllst");


        if((!Utils.checkInternetConnection(getApplicationContext()))||(tvdefaultlist==null)||(tvdefaultlist.size()==0))
        {
            try
            {
                final Handler handler = new Handler();
                Runnable runnable = new Runnable() {
                    int i = 0;
                    public void run() {

                        try
                        {
                            if (i< array_image.size())
                            {
                                imagedefault.setImageResource(array_image.get(i));
                                i++;
                                handler.postDelayed(this,  10000);
                                Log.e("Gallery_i=:",""+i+" size "+array_image.size());
                            }else{
                                // new TvAsync().execute();
                                Log.e("Gallery_i_size=:",""+i);
                                Intent intent =new Intent(getApplicationContext(),VideoViewActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
                                startActivity(intent);
                                finish();
                            }
                        }catch (Throwable e)
                        {

                            Intent intent =new Intent(getApplicationContext(),SplashScreenActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(intent);
                            finish();
                            e.printStackTrace();
                        }

                    }
                };
                handler.postDelayed(runnable, 0);
            }catch (Exception e)
            {
                Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
                Date currentLocalTime = cal.getTime();
                DateFormat date = new SimpleDateFormat("HH:mm a");
                date.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
                String localTime = date.format(currentLocalTime);
                SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_WORLD_READABLE|MODE_WORLD_WRITEABLE).edit();
                editor.putString("name", ""+localTime);
                editor.commit();

                Intent intent =new Intent(getApplicationContext(),SplashScreenActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                finish();
                e.printStackTrace();
            }
        }
        else
        {

            try
            {
                tvdefaultlist= (ArrayList<Gallery>) SessionManager.RetrieveData("galleryllst");

                final Handler handler = new Handler();

                final Runnable runnable = new Runnable() {
                    int i = 0;

                    public void run() {

                        try
                        {
                            if (i< tvdefaultlist.size())
                            {
                                Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
                                Date currentLocalTime = cal.getTime();
                                DateFormat date = new SimpleDateFormat("HH:mm a");
                                date.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
                                String localTime = date.format(currentLocalTime);
                                SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_WORLD_READABLE|MODE_WORLD_WRITEABLE).edit();
                                editor.putString("name", ""+localTime+"Gallery_1");
                                editor.commit();

                                imagedefault.setImageUrl(tvdefaultlist.get(i).getImage());
                            }
                            if (i< tvdefaultlist.size()) {

                                handler.postDelayed(this, tvdefaultlist.get(i).getDuration() * 1000);
                                i++;
                                Log.e("Gallery_i=:",""+i);
                            }

                            if (tvdefaultlist.size() == i){

                                Intent intent =new Intent(getApplicationContext(),VideoViewActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.fadein, R.anim.fadein);
                            }

                            if (i > tvdefaultlist.size() - 1) {

                            }

                        }catch (Exception e)
                        {
                            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
                            Date currentLocalTime = cal.getTime();
                            DateFormat date = new SimpleDateFormat("HH:mm a");
                            date.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
                            String localTime = date.format(currentLocalTime);
                            SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_WORLD_READABLE|MODE_WORLD_WRITEABLE).edit();
                            editor.putString("name", ""+localTime+"gallary_2");
                            editor.commit();

                            Intent intent =new Intent(getApplicationContext(),SplashScreenActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intent);
                            finish();
                            e.printStackTrace();
                        }

                    }
                };
                handler.postDelayed(runnable, 0);
            }catch (Exception e)
            {
                Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
                Date currentLocalTime = cal.getTime();
                DateFormat date = new SimpleDateFormat("HH:mm a");
                date.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
                String localTime = date.format(currentLocalTime);
                SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_WORLD_READABLE|MODE_WORLD_WRITEABLE).edit();
                editor.putString("name", ""+localTime+"Gallery_3");
                editor.commit();

                Intent intent =new Intent(getApplicationContext(),SplashScreenActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                finish();
                e.printStackTrace();
            }
        }

    }

    private class TvAsync extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try{
                commonJsonParsing = new CommonJsonParsing(GalleryActivity.this, "masters");

            }catch (Throwable e)
            {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(Void... params) {


            HashMap hashMap = new HashMap();
            hashMap.put("date", "2016-01-15");


            try {
                result = commonJsonParsing.postJson(hashMap);
            }catch (Throwable e)
            {

                Intent intent =new Intent(getApplicationContext(),QuotesActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fadein, R.anim.fadein);
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            if (result != null) {
                try {

                    object = new JSONObject(result);

                    Log.e("Value1:==", "" + result);
                    Log.d("Value2:==", "" + object);

                    parentarray = object.getJSONArray("news_tech");
                    parentarray1 = object.getJSONArray("quotes");

                    Log.e("news_tech:==", "" + parentarray);
                    Log.e("quotes:==", "" + parentarray1);
                    news_list = new ArrayList<>();

                    for (int i = 0; i < parentarray.length();  i++) {

                        JSONObject finalobject1 = parentarray1.getJSONObject(i);
                        News_tech quotes = new News_tech();
                        quotes.setDuration(finalobject1.getInt("duration"));
                        quotes.setImage(finalobject1.getString("image"));
                        news_list.add(quotes);


                        JSONObject finalobject = parentarray.getJSONObject(i);
                        News_tech tvBean = new News_tech();
                        tvBean.setTitle(finalobject.getString("title"));
                        tvBean.setSubtitle(finalobject.getString("sub_title"));
                        tvBean.setDescription(finalobject.getString("description"));
                        tvBean.setImage(finalobject.getString("image"));
                        tvBean.setDuration(finalobject.getInt("duration"));
                        news_list.add(tvBean);
                    }

                    News_tech news_tech=new News_tech();
                    news_list.add(news_tech);
                    SessionManager.SaveData("newlist",news_list);


                    parentarray = object.getJSONArray("birthday");
                    birthday_list = new ArrayList<>();

                    for (int i = 0; i < parentarray.length(); i++) {

                        JSONObject finalobject = parentarray.getJSONObject(i);
                        Birthday birth = new Birthday();
                        birth.setDuration(finalobject.getInt("duration"));
                        birth.setImage(finalobject.getString("image"));
                        birth.setName(finalobject.getString("name"));
                        birthday_list.add(birth);
                    }

                    Birthday birthday =new Birthday();
                    birthday_list.add(birthday);

                    SessionManager.SaveData("bdaylist",birthday_list);
                    Log.e("Bd_1_size",""+birthday_list.size());

                    parentarray = object.getJSONArray("video");
                    videoList = new ArrayList<>();
                    videoList.clear();


                    for (int i = 0; i < parentarray.length(); i++) {

                        JSONObject finalobject = parentarray.getJSONObject(i);
                        Video video = new Video();
                        video.setDuration(finalobject.getInt("duration"));
                        video.setLink(finalobject.getString("link"));
                        video.setName(finalobject.getString("name"));
                        videoList.add(video);
                    }

                    SessionManager.SaveData("videolist",videoList);

                    parentarray = object.getJSONArray("gallery");
                    Log.e("size_gallery:==", "" + parentarray.length());
                    gallery_list = new ArrayList<Gallery>();
                    gallery_list.clear();
                    final_gallery_list = new ArrayList<>();
                    final_gallery_list.clear();
                    for (int i=0;i<parentarray.length();i++)
                    {
                        JSONObject jsonobject=parentarray.optJSONObject(i);

                        Log.e("JsonValue",""+jsonobject);
                        JSONArray jsonarry=jsonobject.getJSONArray("image");

                        for (int j=0;j<jsonarry.length();j++)
                        {
                            Gallery gallery=new Gallery();
                            JSONObject obj = jsonarry.optJSONObject(j);
                            gallery.setDuration(obj.getInt("duration"));
                            gallery.setImage(obj.getString("image"));
                            gallery_list.add(gallery);
                        }
                    }

                    Gallery gallery=new Gallery();
                    gallery_list.add(gallery);
                    SessionManager.SaveData("galleryllst",gallery_list);


                    parentarray = object.getJSONArray("weather-details");

                    final_wheather_list = new ArrayList<>();
                    final_wheather_list.clear();

                    for (int i=0;i<parentarray.length();i++)
                    {
                        JSONObject jsonobject=parentarray.optJSONObject(i);

                        Wheather wheather=new Wheather();
                        wheather.setCity(jsonobject.getString("city"));
                        wheather.setWheather_report(jsonobject.getString("weather-report"));
                        wheather.setCurr_wheather(jsonobject.getInt("current-weather"));
                        wheather.setMax(jsonobject.getInt("max"));
                        wheather.setMin(jsonobject.getInt("min"));
                        wheather.setBg_image(jsonobject.getString("bg-image"));
                        wheather.setImage(jsonobject.getString("common-image"));
                        wheather.setD1_min(jsonobject.getInt("d1_min"));
                        wheather.setD2_min(jsonobject.getInt("d2_min"));
                        wheather.setD3_min(jsonobject.getInt("d3_min"));
                        wheather.setD4_min(jsonobject.getInt("d4_min"));
                        wheather.setD5_min(jsonobject.getInt("d5_min"));
                        wheather.setD1_max(jsonobject.getInt("d1_max"));
                        wheather.setD2_max(jsonobject.getInt("d2_max"));
                        wheather.setD3_max(jsonobject.getInt("d3_max"));
                        wheather.setD4_max(jsonobject.getInt("d4_max"));
                        wheather.setD5_max(jsonobject.getInt("d5_max"));
                        wheather.setD1_name(jsonobject.getString("d1_name"));
                        wheather.setD2_name(jsonobject.getString("d2_name"));
                        wheather.setD3_name(jsonobject.getString("d3_name"));
                        wheather.setD4_name(jsonobject.getString("d4_name"));
                        wheather.setD5_name(jsonobject.getString("d5_name"));
                        wheather.setD1_image(jsonobject.getString("d1_image"));
                        wheather.setD2_image(jsonobject.getString("d2_image"));
                        wheather.setD3_image(jsonobject.getString("d3_image"));
                        wheather.setD4_image(jsonobject.getString("d4_image"));
                        wheather.setD5_image(jsonobject.getString("d5_image"));
                        wheather.setDuration(jsonobject.getInt("duration"));
                        Log.e("cityname",jsonobject.getString("city"));

                        final_wheather_list.add(wheather);

                    }
                    JSONObject jsonobject=parentarray.optJSONObject(parentarray.length()-1);

                    Wheather wheather=new Wheather();
                    wheather.setCity(jsonobject.getString("city"));
                    wheather.setWheather_report(jsonobject.getString("weather-report"));
                    wheather.setCurr_wheather(jsonobject.getInt("current-weather"));
                    wheather.setMax(jsonobject.getInt("max"));
                    wheather.setMin(jsonobject.getInt("min"));
                    wheather.setBg_image(jsonobject.getString("bg-image"));
                    wheather.setImage(jsonobject.getString("common-image"));
                    wheather.setD1_min(jsonobject.getInt("d1_min"));
                    wheather.setD2_min(jsonobject.getInt("d2_min"));
                    wheather.setD3_min(jsonobject.getInt("d3_min"));
                    wheather.setD4_min(jsonobject.getInt("d4_min"));
                    wheather.setD5_min(jsonobject.getInt("d5_min"));
                    wheather.setD1_max(jsonobject.getInt("d1_max"));
                    wheather.setD2_max(jsonobject.getInt("d2_max"));
                    wheather.setD3_max(jsonobject.getInt("d3_max"));
                    wheather.setD4_max(jsonobject.getInt("d4_max"));
                    wheather.setD5_max(jsonobject.getInt("d5_max"));
                    wheather.setD1_name(jsonobject.getString("d1_name"));
                    wheather.setD2_name(jsonobject.getString("d2_name"));
                    wheather.setD3_name(jsonobject.getString("d3_name"));
                    wheather.setD4_name(jsonobject.getString("d4_name"));
                    wheather.setD5_name(jsonobject.getString("d5_name"));
                    wheather.setD1_image(jsonobject.getString("d1_image"));
                    wheather.setD2_image(jsonobject.getString("d2_image"));
                    wheather.setD3_image(jsonobject.getString("d3_image"));
                    wheather.setD4_image(jsonobject.getString("d4_image"));
                    wheather.setD5_image(jsonobject.getString("d5_image"));
                    Log.e("cityname",jsonobject.getString("city"));

                    final_wheather_list.add(wheather);
                    Log.e("Size",""+final_wheather_list.size());
                    SessionManager.SaveData("Final_wh_list",final_wheather_list);

                    Intent intent =new Intent(getApplicationContext(),QuotesActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fadein, R.anim.fadein);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
