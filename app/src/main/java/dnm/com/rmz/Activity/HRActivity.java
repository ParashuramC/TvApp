package dnm.com.rmz.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import dnm.com.rmz.Bean.Birthday;
import dnm.com.rmz.Bean.HrAnnoucement;
import dnm.com.rmz.Common.CommonJsonParsing;
import dnm.com.rmz.Common.ExceptionHandler;
import dnm.com.rmz.Common.SessionManager;
import dnm.com.rmz.R;
import dnm.com.rmz.Service.TVService;

public class HRActivity extends Activity {

    TextView tvannoucement,tvtxt;
    Context context;
    public static boolean active=false;
    RelativeLayout relativeLayout;
    private TimerTask timer;
    List<HrAnnoucement>  hrAnnoucementList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.hr_annoucement);

        context=this;

        tvannoucement= (TextView) findViewById(R.id.hr_tv_annoucement);
        relativeLayout= (RelativeLayout) findViewById(R.id.rel_hr);

        hrAnnoucementList= (List<HrAnnoucement>) SessionManager.RetrieveData("hrList");

        tvannoucement.setText(hrAnnoucementList.get(0).getMessage());

        timer = new TimerTask() {
            @Override
            public void run() {

                HRActivity.this.finish();
            }
        };
        Timer t = new Timer();
        t.schedule(timer,  hrAnnoucementList.get(0).getDuration() * 1000);
    }

}
