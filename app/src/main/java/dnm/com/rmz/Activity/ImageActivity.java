package dnm.com.rmz.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import dnm.com.rmz.Bean.HrAnnoucement;
import dnm.com.rmz.Common.ExceptionHandler;
import dnm.com.rmz.Common.SessionManager;
import dnm.com.rmz.R;
import dnm.com.rmz.utilities.CustomImageView;

public class ImageActivity extends Activity {

    CustomImageView imagepush;
    List<HrAnnoucement> hrAnnoucementList;
    private TimerTask timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_image);

        imagepush = (CustomImageView) findViewById(R.id.image_push);

        hrAnnoucementList= (List<HrAnnoucement>) SessionManager.RetrieveData("hrList");

        try {
            imagepush.setImageUrl(hrAnnoucementList.get(0).getImage());

            timer = new TimerTask() {
                @Override
                public void run() {

                    ImageActivity.this.finish();
                }
            };
            Timer t = new Timer();
            t.schedule(timer,  hrAnnoucementList.get(0).getDuration() * 1000);
        }catch (Exception e)
        {
            Intent intent =new Intent(getApplicationContext(),SplashScreenActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
            finish();
            e.printStackTrace();
        }

    }
}
