package dnm.com.rmz.Activity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.MediaController;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import dnm.com.rmz.Bean.HrAnnoucement;
import dnm.com.rmz.Common.SessionManager;
import dnm.com.rmz.Database.Rmz_Db;
import dnm.com.rmz.R;
import dnm.com.rmz.Service.TVService;

/**
 * Created by Akshay Raj on 5/31/2016.
 * Snow Corporation Inc.
 * www.snowcorp.org
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    NotificationCompat.Builder notificationBuilder;
    String url="http://r6---sn-p5qs7n7s.googlevideo.com/videoplayback?dur=145.867&mt=1490007741&pl=25&key=yt6&id=o-ADQqOV7WAP4tuS5xAWPqy-fUbtDubnm3Voq8aoMA3j-V&mv=u&ratebypass=yes&source=youtube&ms=au&signature=9AB91B4CD0DA8621BC79DEA1D2F661A2683642C2.7E52CA740797CB52E2AF388BCED77574213800AA&mime=video%2Fmp4&ip=172.241.216.117&mn=sn-p5qs7n7s&mm=31&ipbits=0&lmt=1488457018612343&expire=1490029476&sparams=dur%2Cid%2Cip%2Cipbits%2Citag%2Clmt%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cpl%2Cratebypass%2Csource%2Cupn%2Cexpire&upn=lF2MStx-OxA&itag=22&title=The+Launch+of+RMZ+Infinity+-+Chennai";
    File f,f1;
    List<HrAnnoucement> hrAnnoucementList;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.


        String tag = remoteMessage.getNotification().getTag();
        String msg = remoteMessage.getData().get("message");
        String duration = remoteMessage.getData().get("duration");
        String image = remoteMessage.getData().get("image");
        String flag = remoteMessage.getData().get("flag");
        String  status  = remoteMessage.getData().get("status");
        String  video_url = remoteMessage.getData().get("url");
        String  id = remoteMessage.getData().get("id");
        Log.e(TAG, "From: " + remoteMessage.getData());
        Log.e(TAG, "Message: " + msg);
        Log.e(TAG, "duration: " + duration);
        Log.e(TAG, "image: " + image);
        Log.e(TAG, "status: " + status);
        Log.e(TAG, "video_url: " + video_url);
        Log.e(TAG, "flag: " + flag);
        Log.e(TAG, "id: " + id);
        //sendNotification(tag, msg, image);

        try
        {
            hrAnnoucementList=new ArrayList<>();
            HrAnnoucement hrAnnoucement =new HrAnnoucement();
            hrAnnoucement.setMessage(msg);
            hrAnnoucement.setImage(image);
            hrAnnoucement.setDuration(Integer.parseInt(duration));
            hrAnnoucementList.add(hrAnnoucement);

            SessionManager.SaveData("hrList",hrAnnoucementList);
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        if(flag.equalsIgnoreCase("1"))
        {
            Intent intent1 =new Intent(getBaseContext(),HRActivity.class);
            intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent1);

        }else if (flag.equalsIgnoreCase("2"))
        {
            Intent intent1 =new Intent(getBaseContext(),ImageActivity.class);
            intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent1);

        }else if (flag.equalsIgnoreCase("3"))
        {

            if(status.equalsIgnoreCase("0"))
            {
                Log.e("delited1","video_delited_id :"+id);

                Rmz_Db db=new Rmz_Db(MyFirebaseMessagingService.this);
                db.createDatabase();
                db.openDataBase();
                f1 = new File(String.valueOf(db.getFile_path(Integer.parseInt(id))));
                db.deleteTitle(Integer.parseInt(id));
                f1.delete();

                Log.e("delited","video_delited_id :"+id);

                db.close();

             /*   Intent intent1 =new Intent(getBaseContext(),SplashScreenActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);*/

            }
            else
            {

                new ProgressBack(id,video_url,status).execute();
            }

        }

    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String tag, String messageBody, Bitmap img) {
        Intent intent = new Intent(this, HRActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
/*
        if (tag.equalsIgnoreCase("image")) {
            notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("Firebase Cloud Messaging")
                    .setContentText(messageBody)
                    .setStyle(new NotificationCompat.BigPictureStyle()
                          .bigPicture(img))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
        } else {*/
            notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("Firebase Cloud Messaging")
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
      //  }

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);



        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private class ProgressBack extends AsyncTask<String,String,String> {

        String id,video_url,status;

        public ProgressBack(String id, String video_url, String status) {
            this.id=id;
            this.status=status;
            this.video_url=video_url;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... strings) {

            URL u = null;
            InputStream is = null;

            try
            {


            try {
                Log.e("video_url",video_url);
                u = new URL(video_url);
                is = u.openStream();
                HttpURLConnection huc = (HttpURLConnection)u.openConnection(); //to know the size of video

                int size = huc.getContentLength();

                Log.e("Size",""+size);

                if(huc != null) {
                    String fileName = "RMZ_"+id+".mp4";
                    Log.e("id",id);
                    String storagePath = Environment.getExternalStorageDirectory().getAbsolutePath().toString()+"/RMZ Videos";
                    f = new File(storagePath,fileName);

                    if(!f.exists())
                    {
                        Log.e("File_exists",id+storagePath+ "   "+f.getAbsolutePath());
                        f.getParentFile().mkdirs();
                    }

                    FileOutputStream fos = new FileOutputStream(f);
                    byte[] buffer = new byte[1024];
                    int len1 = 0;
                    if(is != null) {
                        while ((len1 = is.read(buffer)) > 0) {
                            fos.write(buffer,0, len1);
                        }
                        return "success";
                    }
                    if(fos != null) {
                        fos.close();
                    }
                }
            } catch (MalformedURLException mue) {
                mue.printStackTrace();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            } finally {
                try {
                    if(is != null) {
                        is.close();
                    }
                } catch (IOException ioe) {
                    // just going to ignore this one
                }
            }

            }catch (Throwable e)
            {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String s) {


            try
            {
                if(s.equalsIgnoreCase("success"))
                {
                    Rmz_Db db=new Rmz_Db(MyFirebaseMessagingService.this);
                    db.createDatabase();
                    db.openDataBase();

                    db.insert_File_path(Integer.parseInt(id), String.valueOf(f),Integer.parseInt(status));

                    db.close();

                    Log.e("downloaded","video downloaded");

                }
            }catch (Throwable e)
            {
                e.printStackTrace();

            }


           // Toast.makeText(MyFirebaseMessagingService.this, "Downloaded", Toast.LENGTH_SHORT).show();

            super.onPostExecute(s);
        }

    }
}
