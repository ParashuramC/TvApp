package dnm.com.rmz.Activity;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dnm.com.rmz.Bean.News_tech;
import dnm.com.rmz.Common.CommonJsonParsing;
import dnm.com.rmz.Common.SessionManager;
import dnm.com.rmz.R;
import dnm.com.rmz.utilities.CustomImageView;

public class NewsActivity extends Activity {


    TextView tv_title, tv_sub_tittle, tv_description, tv_image_below;
    RelativeLayout relativeLayout;
    CustomImageView image_an;
    Animation animationFadeIn,animationFadeOut;
    Handler handler;
    private List<News_tech> tvnewslist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_announcement);

        tv_title = (TextView) findViewById(R.id.an_tv_title);
        tv_sub_tittle = (TextView) findViewById(R.id.an_tv_sub_title);
        tv_description = (TextView) findViewById(R.id.an_tv_description);
        image_an = (CustomImageView) findViewById(R.id.an_image);
        relativeLayout= (RelativeLayout) findViewById(R.id.rel);

        animationFadeIn = AnimationUtils.loadAnimation(NewsActivity.this, R.anim.fadein);
        animationFadeOut = AnimationUtils.loadAnimation(NewsActivity.this, R.anim.fadeout);

        handler = new Handler();
        tvnewslist =new ArrayList<>();

        tvnewslist= (List<News_tech>) SessionManager.RetrieveData("newlist");

        try
        {

        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            int i = 0;

            public void run() {

                try{

                tv_title.setText(tvnewslist.get(i).getTitle());
                tv_sub_tittle.setText(tvnewslist.get(i).getSubtitle());
                tv_description.setText(tvnewslist.get(i).getDescription());
                image_an.setImageUrl(tvnewslist.get(i).getImage());
                i++;
                if (i > tvnewslist.size() - 1) {
                    i = 0;

                }
                if(i%2==0)
                {
                    relativeLayout.startAnimation(animationFadeIn);
                }
                else
                {
                    relativeLayout.startAnimation(animationFadeOut);
                }

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                handler.postDelayed(this, 20000);

            }
        };
        handler.postDelayed(runnable, 0);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }


}
