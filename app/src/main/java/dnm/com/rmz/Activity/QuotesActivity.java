package dnm.com.rmz.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.TimerTask;

import dnm.com.rmz.Bean.Birthday;
import dnm.com.rmz.Bean.News_tech;
import dnm.com.rmz.Bean.HrAnnoucement;
import dnm.com.rmz.Common.CommonJsonParsing;
import dnm.com.rmz.Common.ExceptionHandler;
import dnm.com.rmz.Common.SessionManager;
import dnm.com.rmz.R;
import dnm.com.rmz.utilities.CustomImageView;
import dnm.com.rmz.utilities.Utils;


public class QuotesActivity extends ActionBarActivity {

    List<News_tech> tvQuoteslist;
    CustomImageView imagequote,image_an;
    TextView tv_title, tv_sub_tittle, tv_description, tv_image_below;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        tvQuoteslist=new ArrayList<>();

        tvQuoteslist= (List<News_tech>) SessionManager.RetrieveData("newlist");

        if((!Utils.checkInternetConnection(getApplicationContext()))||(tvQuoteslist==null)||(tvQuoteslist.size()==0))
        {
            Intent intent =new Intent(getApplicationContext(),SplashScreenActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.fadein, R.anim.fadein);

        }

        else
        {

            try{

        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {

            int i = 0;

            public void run() {

                try{

                if (i< tvQuoteslist.size())
                {
                    if(i%2==0)
                    {
                        setContentView(R.layout.quotes_im_fragment);
                        imagequote = (CustomImageView) findViewById(R.id.quotes_images);
                        imagequote.setImageUrl(tvQuoteslist.get(i).getImage());

                    }
                    else if (i%2==1)
                    {
                        setContentView(R.layout.activity_announcement);
                        tv_title = (TextView) findViewById(R.id.an_tv_title);
                        tv_sub_tittle = (TextView) findViewById(R.id.an_tv_sub_title);
                        tv_description = (TextView) findViewById(R.id.an_tv_description);
                        image_an = (CustomImageView) findViewById(R.id.an_image);
                        tv_title.setText(tvQuoteslist.get(i).getTitle());
                        tv_sub_tittle.setText(tvQuoteslist.get(i).getSubtitle());
                        tv_description.setText(tvQuoteslist.get(i).getDescription());
                        image_an.setImageUrl(tvQuoteslist.get(i).getImage());

                    }
                }

                if (i< tvQuoteslist.size()) {

                    handler.postDelayed(this, tvQuoteslist.get(i).getDuration() * 1000);
                    i++;
                    Log.e("quotes_i==:",""+i);
                }
                if (tvQuoteslist.size() == i){

                    Intent intent =new Intent(getApplicationContext(),WheatherActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY| Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fadein, R.anim.fadein);

                }

                if (i > tvQuoteslist.size() - 1) {

                }
                }catch (Exception e)
                {
                    Intent intent =new Intent(getApplicationContext(),SplashScreenActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                    finish();
                    e.printStackTrace();
                }
            }
        };
        handler.postDelayed(runnable, 0);
        }catch (Exception e)
        {
            Intent intent =new Intent(getApplicationContext(),SplashScreenActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
            finish();
            e.printStackTrace();
        }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        finish();
    }

}
