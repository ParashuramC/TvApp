package dnm.com.rmz.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import dnm.com.rmz.AppContext;
import dnm.com.rmz.Bean.Birthday;
import dnm.com.rmz.Bean.Gallery;
import dnm.com.rmz.Bean.News_tech;
import dnm.com.rmz.Bean.Video;
import dnm.com.rmz.Bean.Wheather;
import dnm.com.rmz.Common.CommonJsonParsing;
import dnm.com.rmz.Common.ExceptionHandler;
import dnm.com.rmz.Common.SessionManager;
import dnm.com.rmz.PrefUtils;
import dnm.com.rmz.R;
import dnm.com.rmz.Service.TVService;
import io.fabric.sdk.android.Fabric;


public class SplashScreenActivity extends Activity {

    Context context;
    private final List blockedKeys = new ArrayList(Arrays.asList(KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP));
    TimerTask timer;
    ImageView imagesplash;
    CommonJsonParsing commonJsonParsing;
    private String result;
    List<News_tech> news_list;
    List<Video> videoList;
    ArrayList<Gallery> gallery_list;
    ArrayList final_gallery_list;
    List<Wheather> final_wheather_list;
    List<Birthday> birthday_list;
    public static String BASE_URL = "http://thebay.in/androidtv/application/views/admin/register.php";
  //  public static String BASE_URL = "http://dnmdigital.com/projects/androidtv/application/views/admin/register.php";
    private static String TAG = "SplashScreenActivity";
    JSONObject object;
    JSONArray parentarray;
    private JSONArray parentarray1;
    public static final String PREFS_NAME = "MyApp_Settings";
    ArrayList<Integer> array_image;
    //com.mitv.tvhome
    //unfortunately device report has stopped
    //https://www.google.co.in/search?q=ConnectivityService%3A+RemoteException+caught+trying+to+send+a+callback+msg+for+NetworkRequest+%5B+id%3D2%2C+legacyType%3D-1%2C+%5B+Capabilities%3A+INTERNET%26NOT_RESTRICTED%26TRUSTED%26NOT_VPN%5D+%5D&oq=ConnectivityService%3A+RemoteException+caught+trying+to+send+a+callback+msg+for+NetworkRequest+%5B+id%3D2%2C+legacyType%3D-1%2C+%5B+Capabilities%3A+INTERNET%26NOT_RESTRICTED%26TRUSTED%26NOT_VPN%5D+%5D&aqs=chrome..69i57j69i58.962j0j1&client=ubuntu&sourceid=chrome&ie=UTF-8
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(SplashScreenActivity.this, new Crashlytics());

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        context = this;

       // new AppContext().startKioskService();
        //PrefUtils.setKioskModeActive(true, getApplicationContext());
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_WORLD_READABLE|MODE_WORLD_WRITEABLE);
        String restoredText = prefs.getString("name", null);
        if (restoredText != null) {
            String name = prefs.getString("name", "No name defined");
            Log.e("Name_para",""+name);
        }

        clearCache();
      //  MyApplication.getInstance().clearApplicationData();


        imagesplash = (ImageView) findViewById(R.id.splash_image);
        array_image = new ArrayList<Integer>();
        array_image.add(R.drawable.rmz_1);
        array_image.add(R.drawable.rmz_2);
        array_image.add(R.drawable.rmz_3);
        array_image.add(R.drawable.rmz_4);
        array_image.add(R.drawable.rmz_4);

        SessionManager.SaveData("Image_set",array_image);

        timer = new TimerTask() {
            @Override
            public void run() {

                registerToServer();
                Log.e("Feed On",""+"Feed On");
            }
        };
        Timer t = new Timer();
        t.schedule(timer, 5000);

        try
        {
            new TvAsync().execute();
        }catch (Throwable e)
        {
            e.printStackTrace();
        }


    }

   /* @Override
    protected void onResume() {

                Intent intent = new Intent(context, SplashScreenActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fadein, R.anim.fadeout);


        super.onResume();
    }*/
   public void clearCache() {
       Log.i(TAG, "Clearing Cache.");
       File[] dir = context.getCacheDir().listFiles();
       if(dir != null){
           for (File f : dir){
               f.delete();
           }
       }
   }


    private void registerToServer() {
        // Tag used to cancel the request
        String tag_string_req = "req_register";



        StringRequest strReq = new StringRequest(Request.Method.POST,
                BASE_URL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e("Ree: ","" + response.toString());


                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                       // Toast.makeText(getApplicationContext(), "Register Successful!", Toast.LENGTH_LONG).show();
                        /*Intent i = new Intent(SplashScreenActivity.this, SplashScreenActivity.class);
                        i.putExtra("name", "");
                        startActivity(i);*/
                    } else {
                        //Toast.makeText(getApplicationContext(), "this email is already in use", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Database Problem!", Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Register Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                String fcm_id = FirebaseInstanceId.getInstance().getToken();

                Log.e("Fcm",""+fcm_id);

                params.put("fcm_id", fcm_id);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }

        };

        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(!hasFocus) {
            // Close every kind of system dialog
            Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            sendBroadcast(closeDialog);
        }
    }

    @Override
    public void onBackPressed() {
        // nothing to do here
        // … really
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (blockedKeys.contains(event.getKeyCode())) {
            return true;
        } else {
            return super.dispatchKeyEvent(event);
        }
    }

    private class TvAsync extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try{
                commonJsonParsing = new CommonJsonParsing(SplashScreenActivity.this, "masters");

            }catch (Throwable e)
            {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(Void... params) {


            HashMap hashMap = new HashMap();
            hashMap.put("date", "2016-01-15");


            try {
                result = commonJsonParsing.postJson(hashMap);
            }catch (Throwable e)
            {
                timer = new TimerTask() {
                    @Override
                    public void run() {
                        stopService(new Intent(SplashScreenActivity.this, TVService.class));
                        Intent intent = new Intent(context, GalleryActivity.class);
                        startService(new Intent(getApplicationContext(), TVService.class));
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.fadein, R.anim.fadeout);

                    }
                };

                Timer t = new Timer();
                t.schedule(timer, 6000);
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (result != null) {
                try {

                    object = new JSONObject(result);

                    Log.e("Value1:==", "" + result);
                    Log.d("Value2:==", "" + object);

                    parentarray = object.getJSONArray("news_tech");
                    parentarray1 = object.getJSONArray("quotes");

                    Log.e("news_tech:==", "" + parentarray);
                    Log.e("quotes:==", "" + parentarray1);
                    news_list = new ArrayList<>();

                    for (int i = 0; i < parentarray.length();  i++) {

                        JSONObject finalobject1 = parentarray1.getJSONObject(i);
                        News_tech quotes = new News_tech();
                        quotes.setDuration(finalobject1.getInt("duration"));
                        quotes.setImage(finalobject1.getString("image"));
                        news_list.add(quotes);


                        JSONObject finalobject = parentarray.getJSONObject(i);
                        News_tech tvBean = new News_tech();
                        tvBean.setTitle(finalobject.getString("title"));
                        tvBean.setSubtitle(finalobject.getString("sub_title"));
                        tvBean.setDescription(finalobject.getString("description"));
                        tvBean.setImage(finalobject.getString("image"));
                        tvBean.setDuration(finalobject.getInt("duration"));
                        news_list.add(tvBean);
                    }

                    News_tech news_tech=new News_tech();
                    news_list.add(news_tech);
                    SessionManager.SaveData("newlist",news_list);


                    parentarray = object.getJSONArray("birthday");
                    birthday_list = new ArrayList<>();

                    for (int i = 0; i < parentarray.length(); i++) {

                        JSONObject finalobject = parentarray.getJSONObject(i);
                        Birthday birth = new Birthday();
                        birth.setDuration(finalobject.getInt("duration"));
                        birth.setImage(finalobject.getString("image"));
                        birth.setName(finalobject.getString("name"));
                        birthday_list.add(birth);
                    }

                    Birthday birthday =new Birthday();
                    birthday_list.add(birthday);

                    SessionManager.SaveData("bdaylist",birthday_list);
                    Log.e("Bd_1_size",""+birthday_list.size());

                   /* parentarray = object.getJSONArray("video");
                    videoList = new ArrayList<>();
                    videoList.clear();


                    for (int i = 0; i < parentarray.length(); i++) {

                        JSONObject finalobject = parentarray.getJSONObject(i);
                        Video video = new Video();
                        video.setDuration(finalobject.getInt("duration"));
                        video.setLink(finalobject.getString("link"));
                        video.setName(finalobject.getString("name"));
                        videoList.add(video);
                    }

                    SessionManager.SaveData("videolist",videoList);
*/
                    parentarray = object.getJSONArray("gallery");
                    Log.e("size_gallery:==", "" + parentarray.length());
                    gallery_list = new ArrayList<Gallery>();
                    gallery_list.clear();
                    final_gallery_list = new ArrayList<>();
                    final_gallery_list.clear();
                      for (int i=0;i<parentarray.length();i++)
                      {
                         // Gallery object = new Gallery();
                         JSONObject jsonobject=parentarray.optJSONObject(i);

                          Log.e("JsonValue",""+jsonobject);
                          JSONArray jsonarry=jsonobject.getJSONArray("image");

                          for (int j=0;j<jsonarry.length();j++)
                          {
                              Gallery gallery=new Gallery();
                              JSONObject obj = jsonarry.optJSONObject(j);
                              gallery.setDuration(obj.getInt("duration"));
                              gallery.setImage(obj.getString("image"));
                              //Log.e("imageeeee",""+obj.getString("image"));
                              gallery_list.add(gallery);
                          }
                          //object.setImage_list(gallery_list);
                          //final_gallery_list.add(object)
                      }

                    Gallery gallery=new Gallery();
                    gallery_list.add(gallery);
                    SessionManager.SaveData("galleryllst",gallery_list);


                    parentarray = object.getJSONArray("weather-details");

                    final_wheather_list = new ArrayList<>();
                    final_wheather_list.clear();

                    for (int i=0;i<parentarray.length();i++)
                    {
                        JSONObject jsonobject=parentarray.optJSONObject(i);

                        Wheather wheather=new Wheather();
                        wheather.setCity(jsonobject.getString("city"));
                        wheather.setWheather_report(jsonobject.getString("weather-report"));
                        wheather.setCurr_wheather(jsonobject.getInt("current-weather"));
                        wheather.setMax(jsonobject.getInt("max"));
                        wheather.setMin(jsonobject.getInt("min"));
                        wheather.setBg_image(jsonobject.getString("bg-image"));
                        wheather.setImage(jsonobject.getString("common-image"));
                        wheather.setD1_min(jsonobject.getInt("d1_min"));
                        wheather.setD2_min(jsonobject.getInt("d2_min"));
                        wheather.setD3_min(jsonobject.getInt("d3_min"));
                        wheather.setD4_min(jsonobject.getInt("d4_min"));
                        wheather.setD5_min(jsonobject.getInt("d5_min"));
                        wheather.setD1_max(jsonobject.getInt("d1_max"));
                        wheather.setD2_max(jsonobject.getInt("d2_max"));
                        wheather.setD3_max(jsonobject.getInt("d3_max"));
                        wheather.setD4_max(jsonobject.getInt("d4_max"));
                        wheather.setD5_max(jsonobject.getInt("d5_max"));
                        wheather.setD1_name(jsonobject.getString("d1_name"));
                        wheather.setD2_name(jsonobject.getString("d2_name"));
                        wheather.setD3_name(jsonobject.getString("d3_name"));
                        wheather.setD4_name(jsonobject.getString("d4_name"));
                        wheather.setD5_name(jsonobject.getString("d5_name"));
                        wheather.setD1_image(jsonobject.getString("d1_image"));
                        wheather.setD2_image(jsonobject.getString("d2_image"));
                        wheather.setD3_image(jsonobject.getString("d3_image"));
                        wheather.setD4_image(jsonobject.getString("d4_image"));
                        wheather.setD5_image(jsonobject.getString("d5_image"));
                        wheather.setDuration(jsonobject.getInt("duration"));
                        Log.e("cityname",jsonobject.getString("city"));

                        final_wheather_list.add(wheather);

                    }
                    JSONObject jsonobject=parentarray.optJSONObject(parentarray.length()-1);

                    Wheather wheather=new Wheather();
                    wheather.setCity(jsonobject.getString("city"));
                    wheather.setWheather_report(jsonobject.getString("weather-report"));
                    wheather.setCurr_wheather(jsonobject.getInt("current-weather"));
                    wheather.setMax(jsonobject.getInt("max"));
                    wheather.setMin(jsonobject.getInt("min"));
                    wheather.setBg_image(jsonobject.getString("bg-image"));
                    wheather.setImage(jsonobject.getString("common-image"));
                    wheather.setD1_min(jsonobject.getInt("d1_min"));
                    wheather.setD2_min(jsonobject.getInt("d2_min"));
                    wheather.setD3_min(jsonobject.getInt("d3_min"));
                    wheather.setD4_min(jsonobject.getInt("d4_min"));
                    wheather.setD5_min(jsonobject.getInt("d5_min"));
                    wheather.setD1_max(jsonobject.getInt("d1_max"));
                    wheather.setD2_max(jsonobject.getInt("d2_max"));
                    wheather.setD3_max(jsonobject.getInt("d3_max"));
                    wheather.setD4_max(jsonobject.getInt("d4_max"));
                    wheather.setD5_max(jsonobject.getInt("d5_max"));
                    wheather.setD1_name(jsonobject.getString("d1_name"));
                    wheather.setD2_name(jsonobject.getString("d2_name"));
                    wheather.setD3_name(jsonobject.getString("d3_name"));
                    wheather.setD4_name(jsonobject.getString("d4_name"));
                    wheather.setD5_name(jsonobject.getString("d5_name"));
                    wheather.setD1_image(jsonobject.getString("d1_image"));
                    wheather.setD2_image(jsonobject.getString("d2_image"));
                    wheather.setD3_image(jsonobject.getString("d3_image"));
                    wheather.setD4_image(jsonobject.getString("d4_image"));
                    wheather.setD5_image(jsonobject.getString("d5_image"));
                    Log.e("cityname",jsonobject.getString("city"));

                    final_wheather_list.add(wheather);
                    Log.e("Size",""+final_wheather_list.size());
                    SessionManager.SaveData("Final_wh_list",final_wheather_list);

                    timer = new TimerTask() {
                        @Override
                        public void run() {
                            stopService(new Intent(SplashScreenActivity.this, TVService.class));
                            Intent intent = new Intent(context, GalleryActivity.class);
                            startService(new Intent(getApplicationContext(), TVService.class));
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.fadein, R.anim.fadeout);

                        }
                    };

                    Timer t = new Timer();
                    t.schedule(timer, 6000);

                } catch (Exception e) {
                    timer = new TimerTask() {
                        @Override
                        public void run() {
                            stopService(new Intent(SplashScreenActivity.this, TVService.class));
                            Intent intent = new Intent(context, GalleryActivity.class);
                            startService(new Intent(getApplicationContext(), TVService.class));
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.fadein, R.anim.fadeout);

                        }
                    };

                    Timer t = new Timer();
                    t.schedule(timer, 6000);
                    e.printStackTrace();
                }
            }
        }
    }
}

