

package dnm.com.rmz.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v17.leanback.widget.Util;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;
import java.util.ArrayList;
import java.util.List;
import dnm.com.rmz.Bean.Video;
import dnm.com.rmz.Common.ExceptionHandler;
import dnm.com.rmz.Database.Rmz_Db;
import dnm.com.rmz.R;
import dnm.com.rmz.utilities.Utils;

public class VideoViewActivity extends Activity {

    VideoView videoView;
    Context context;
    int id;
    String str1,str=null;
    //video can not play
    List<Video> list_videos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_video_view);

        context=this;
        list_videos=new ArrayList<>();
        videoView= (VideoView) findViewById(R.id.video_view);
        MediaController mediaController= new MediaController(VideoViewActivity.this);
        mediaController.setAnchorView(videoView);
        mediaController.setVisibility(View.GONE);

        try
        {

        Rmz_Db db=new Rmz_Db(context);
        db.createDatabase();
        db.openDataBase();

            Cursor cursor=db.getAll_IDs();
            list_videos.clear();

            Log.e("count_cursor" ,""+cursor.getCount());

            if(cursor.moveToFirst())
            {
                for(int i=0; i<cursor.getCount();i++)
                {
                    Log.e("count_cusor_id_1",""+cursor.getInt(cursor.getColumnIndex("id")));
                    Video video=new Video();
                    video.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    list_videos.add(video);
                    cursor.moveToNext();
                }
            }

            Log.e("count_cusor_id",""+list_videos.get(0).getId());

            id=0;

            str   = String.valueOf(db.getFile_path(list_videos.get(id).getId()));
            Uri uri2=Uri.parse(Environment.getExternalStorageDirectory().getPath()+"/RMZ Videos");
            String str23=String.valueOf(uri2)+"/RMZ_"+list_videos.get(id).getId()+".mp4";
            Log.e("str_path",str);
            Log.e("str_path4",str23);

        if(str.equalsIgnoreCase(str23))
        {
                Uri uri=Uri.parse(str);
                videoView.setMediaController(mediaController);
                videoView.setVideoURI(uri);
                videoView.requestFocus();
                videoView.start();
        }
        else
        {
            if(!Utils.checkInternetConnection(getApplicationContext()))
            {
                Intent intent =new Intent(getApplicationContext(),SplashScreenActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                finish();
            }
            else
            {
                Log.e("count_cusor_id_3","completed"+list_videos.size());
                Intent intent =new Intent(getApplicationContext(),GalleryActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                finish();
            }

        }

            db.close();

        }catch (Throwable e)
        {
            if(!Utils.checkInternetConnection(getApplicationContext()))
            {
                Intent intent =new Intent(getApplicationContext(),SplashScreenActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                finish();
            }
            else
            {
                Log.e("count_cusor_id_3","completed"+list_videos.size());
                Intent intent =new Intent(getApplicationContext(),GalleryActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                finish();
            }

        }

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {

                try
                {

                id=id+1;
                if(id<list_videos.size())
                {
                    Log.e("id",""+id);
                    Rmz_Db db=new Rmz_Db(context);
                    db.createDatabase();
                    db.openDataBase();
                    str1 = String.valueOf(db.getFile_path(list_videos.get(id).getId()));
                    Log.e("str_path23",str1);
                    Uri uri2=Uri.parse(Environment.getExternalStorageDirectory().getPath()+"/RMZ Videos");
                    String str23=String.valueOf(uri2)+"/RMZ_"+list_videos.get(id).getId()+".mp4";
                    Log.e("id2",""+id);

                    Log.e("count_cusor_id_2",""+list_videos.get(id).getId());

                    if(str1.equalsIgnoreCase(str23))
                    {
                        Log.e("count_cusor_id_3","playingg"+list_videos.size());
                        MediaController mediaController= new MediaController(VideoViewActivity.this);
                        mediaController.setAnchorView(videoView);
                        mediaController.setVisibility(View.GONE);
                        Uri uri=Uri.parse(str1);
                        videoView.setMediaController(mediaController);
                        videoView.setVideoURI(uri);
                        videoView.requestFocus();
                        videoView.start();

                    }
                    else
                    {

                        if(!Utils.checkInternetConnection(getApplicationContext()))
                        {
                            Intent intent =new Intent(getApplicationContext(),SplashScreenActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intent);
                            finish();
                        }
                        else
                        {
                            Log.e("count_cusor_id_3","completed"+list_videos.size());
                            Intent intent =new Intent(getApplicationContext(),GalleryActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intent);
                            finish();
                        }

                    }

                    db.close();
                }
                else
                {
                    if(!Utils.checkInternetConnection(getApplicationContext()))
                    {
                        Intent intent =new Intent(getApplicationContext(),SplashScreenActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
                        Log.e("count_cusor_id_3","completed"+list_videos.size());
                        Intent intent =new Intent(getApplicationContext(),GalleryActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);
                        finish();
                    }
                }
                }catch (Throwable e)
                {
                    e.printStackTrace();
                }


            }
        });

    }
}
