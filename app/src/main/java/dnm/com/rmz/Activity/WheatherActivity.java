package dnm.com.rmz.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import dnm.com.rmz.Bean.DayWhether;
import dnm.com.rmz.Bean.Wheather;
import dnm.com.rmz.Common.ExceptionHandler;
import dnm.com.rmz.Common.SessionManager;
import dnm.com.rmz.R;
import dnm.com.rmz.utilities.CustomImageView;
import dnm.com.rmz.utilities.Utils;

public class WheatherActivity extends ActionBarActivity {

    TextView tv_city_name, tv_degree, tv_min_degree, tv_max_degree,
            tv_wheather_report, tv_week_name1, tv_week_name2, tv_week_name3,
            tv_week_name4, tv_week_name5, tv_d_min_degree1, tv_d_min_degree2,
            tv_d_min_degree3, tv_d_min_degree4, tv_d_min_degree5, tv_d_max_degree1,
            tv_d_max_degree2, tv_d_max_degree3, tv_d_max_degree4, tv_d_max_degree5;

    CustomImageView im_big_image, im_small1, im_small2, im_small3, im_small4, im_small5, im_template;
    public static final String PREFS_NAME = "MyApp_Settings";
    List<Wheather> whether_list;
    List<DayWhether> Day_whether_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_wheather);

        tv_city_name = (TextView) findViewById(R.id.w_tv_city_name);
        tv_degree = (TextView) findViewById(R.id.w_tv_current_degree);
        tv_wheather_report = (TextView) findViewById(R.id.w_tv_wheather_report);
        tv_week_name1 = (TextView) findViewById(R.id.w_tv_week_name1);
        tv_week_name2 = (TextView) findViewById(R.id.w_tv_week_name2);
        tv_week_name3 = (TextView) findViewById(R.id.w_tv_week_name3);
        tv_week_name4 = (TextView) findViewById(R.id.w_tv_week_name4);
        tv_week_name5 = (TextView) findViewById(R.id.w_tv_week_name5);
        tv_d_min_degree1 = (TextView) findViewById(R.id.w_tv_d_min_degree1);
        tv_d_min_degree2 = (TextView) findViewById(R.id.w_tv_d_min_degree2);
        tv_d_min_degree3 = (TextView) findViewById(R.id.w_tv_d_min_degree3);
        tv_d_min_degree4 = (TextView) findViewById(R.id.w_tv_d_min_degree4);
        tv_d_min_degree5 = (TextView) findViewById(R.id.w_tv_d_min_degree5);
        tv_d_max_degree1 = (TextView) findViewById(R.id.w_tv_d_max_degree1);
        tv_d_max_degree2 = (TextView) findViewById(R.id.w_tv_d_max_degree2);
        tv_d_max_degree3 = (TextView) findViewById(R.id.w_tv_d_max_degree3);
        tv_d_max_degree4 = (TextView) findViewById(R.id.w_tv_d_max_degree4);
        tv_d_max_degree5 = (TextView) findViewById(R.id.w_tv_d_max_degree5);
        im_big_image = (CustomImageView) findViewById(R.id.w_im_big);
        im_small1 = (CustomImageView) findViewById(R.id.w_im_small_image1);
        im_small2 = (CustomImageView) findViewById(R.id.w_im_small_image2);
        im_small3 = (CustomImageView) findViewById(R.id.w_im_small_image3);
        im_small4 = (CustomImageView) findViewById(R.id.w_im_small_image4);
        im_small5 = (CustomImageView) findViewById(R.id.w_im_small_image5);
        im_template = (CustomImageView) findViewById(R.id.w_im_template);

        Day_whether_list = new ArrayList<>();
        whether_list = (ArrayList<Wheather>) SessionManager.RetrieveData("Final_wh_list");

        if((!Utils.checkInternetConnection(getApplicationContext()))||(whether_list==null)||(whether_list.size()==0))
        {
            Intent intent = new Intent(getApplicationContext(), SplashScreenActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.fadein, R.anim.fadein);

        }
        else
        {

        try{

        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            int i = 0;

            public void run() {

                try
                {

                if (i < whether_list.size()) {


                    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
                    Date currentLocalTime = cal.getTime();
                    DateFormat date = new SimpleDateFormat("HH:mm a");
                    date.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
                    String localTime = date.format(currentLocalTime);
                    SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_WORLD_READABLE|MODE_WORLD_WRITEABLE).edit();
                    editor.putString("name", ""+localTime+"whether_load_1");
                    editor.commit();

                    im_template.setImageUrl(whether_list.get(i).getBg_image());
                    im_big_image.setImageUrl(whether_list.get(i).getImage());
                    tv_city_name.setText(whether_list.get(i).getCity());
                    tv_wheather_report.setText(whether_list.get(i).getWheather_report());
                    tv_degree.setText(String.valueOf(whether_list.get(i).getCurr_wheather())+"\u00B0");
                    tv_d_max_degree1.setText(String.valueOf(whether_list.get(i).getD1_max())+"\u00B0");
                    tv_d_max_degree2.setText(String.valueOf(whether_list.get(i).getD2_max())+"\u00B0");
                    tv_d_max_degree3.setText(String.valueOf(whether_list.get(i).getD3_max())+"\u00B0");
                    tv_d_max_degree4.setText(String.valueOf(whether_list.get(i).getD4_max())+"\u00B0");
                    tv_d_max_degree5.setText(String.valueOf(whether_list.get(i).getD5_max())+"\u00B0");
                    tv_d_min_degree1.setText(String.valueOf(whether_list.get(i).getD1_min())+"\u00B0");
                    tv_d_min_degree2.setText(String.valueOf(whether_list.get(i).getD2_min())+"\u00B0");
                    tv_d_min_degree3.setText(String.valueOf(whether_list.get(i).getD3_min())+"\u00B0");
                    tv_d_min_degree4.setText(String.valueOf(whether_list.get(i).getD4_min())+"\u00B0");
                    tv_d_min_degree5.setText(String.valueOf(whether_list.get(i).getD5_min())+"\u00B0");
                    tv_week_name1.setText(whether_list.get(i).getD1_name());
                    tv_week_name2.setText(whether_list.get(i).getD2_name());
                    tv_week_name3.setText(whether_list.get(i).getD3_name());
                    tv_week_name4.setText(whether_list.get(i).getD4_name());
                    tv_week_name5.setText(whether_list.get(i).getD5_name());
                    im_small1.setImageUrl(whether_list.get(i).getD1_image());
                    im_small2.setImageUrl(whether_list.get(i).getD2_image());
                    im_small3.setImageUrl(whether_list.get(i).getD3_image());
                    im_small4.setImageUrl(whether_list.get(i).getD4_image());
                    im_small5.setImageUrl(whether_list.get(i).getD5_image());
                }

                if (i < whether_list.size()) {

                    handler.postDelayed(this, whether_list.get(i).getDuration() * 1000);
                    i++;

                    Log.e("Whether_i=:",""+i);

                }
                if (whether_list.size() == i) {



                    Intent intent = new Intent(getApplicationContext(), VideoViewActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY| Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fadein, R.anim.fadein);

                }

                if (i > whether_list.size() - 1) {


                }
                }catch (Exception e)
                {

                    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
                    Date currentLocalTime = cal.getTime();
                    DateFormat date = new SimpleDateFormat("HH:mm a");
                    date.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
                    String localTime = date.format(currentLocalTime);
                    SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_WORLD_READABLE|MODE_WORLD_WRITEABLE).edit();
                    editor.putString("name", ""+localTime+"whether_load_2");
                    editor.commit();

                    Intent intent =new Intent(getApplicationContext(),SplashScreenActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                    finish();
                    e.printStackTrace();
                }
            }
        };
        handler.postDelayed(runnable, 0);
        }catch (Exception e)
        {
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
            Date currentLocalTime = cal.getTime();
            DateFormat date = new SimpleDateFormat("HH:mm a");
            date.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
            String localTime = date.format(currentLocalTime);
            SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_WORLD_READABLE|MODE_WORLD_WRITEABLE).edit();
            editor.putString("name", ""+localTime+"whether_load_3");
            editor.commit();

            Intent intent =new Intent(getApplicationContext(),SplashScreenActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
            finish();
            e.printStackTrace();
        }
    }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        finish();
    }

}