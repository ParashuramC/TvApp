package dnm.com.rmz.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

import dnm.com.rmz.Bean.Video;
import dnm.com.rmz.Common.ExceptionHandler;
import dnm.com.rmz.Common.SessionManager;
import dnm.com.rmz.R;
import dnm.com.rmz.Service.TVService;
import dnm.com.rmz.utilities.Utils;

public class YoutubeVideoActivity extends YouTubeBaseActivity implements  YouTubePlayer.OnInitializedListener{

    int i,flag=0;
    List<Video> videoList;
    private YouTubePlayerView youTubeView;
    YouTubePlayer player1;
    public static final String API_KEY = "AIzaSyBKcuDi0YxqESxIE4RKEEKCdIzVVeuDl4c";
    int second=0;
   // public static final String VIDEO_ID = "AG_gRCBmk_E";
    private TimerTask timer;
    Handler    handler;
    Runnable runnable;
    Handler    handler1;
    Runnable runnable1;
    public static final String PREFS_NAME = "MyApp_Settings";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_video);

        youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_player);
        videoList= (List<Video>) SessionManager.RetrieveData("videolist");
        youTubeView.initialize(API_KEY, this);

    }
    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean restored) {

        this.player1=player;
        try
        {
        if (restored) {
            player1.play();
            try
            {
                handler=  new Handler();
                handler.removeCallbacks(runnable);

            }catch (Exception e)
            {
                e.printStackTrace();
            }


        } else {
            player1.loadVideo(videoList.get(i).getLink());
        }
        player1.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION);
        player1.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE);
        player1.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);
        player1.setFullscreen(true);
        player1.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
        player1.setPlayerStateChangeListener(playerStateChangeListener);
        player1.setPlaybackEventListener(playbackEventListener);

        }catch (Exception e)
        {

            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
            Date currentLocalTime = cal.getTime();
            DateFormat date = new SimpleDateFormat("HH:mm a");
            date.setTimeZone(TimeZone.getTimeZone("GMT+1:00"));
            String localTime = date.format(currentLocalTime);
            SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_WORLD_READABLE|MODE_WORLD_WRITEABLE).edit();
            editor.putString("name", ""+localTime);
            editor.commit();

            Intent intent =new Intent(getApplicationContext(),GalleryActivity.class);
            //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
            finish();
            e.printStackTrace();
        }
    }

        @Override
        public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
            Date currentLocalTime = cal.getTime();
            DateFormat date = new SimpleDateFormat("HH:mm a");
            date.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
            String localTime = date.format(currentLocalTime);
            SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_WORLD_READABLE|MODE_WORLD_WRITEABLE).edit();
            editor.putString("name", ""+localTime+"video_1");
            editor.commit();

            Intent intent =new Intent(getApplicationContext(),GalleryActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.fadeout, R.anim.fadein);
        }

        private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {

            @Override
            public void onBuffering(boolean arg0) {
              //  Toast.makeText(getApplicationContext(),"onBuffering",Toast.LENGTH_LONG);

            }

            @Override
            public void onPaused() {
            }

            @Override
            public void onPlaying() {
                try
                {
                    handler.removeCallbacks(runnable);
                    handler.removeCallbacksAndMessages(null);

                }catch (Exception e)
                {
                    e.printStackTrace();
                }

            }

            @Override
            public void onSeekTo(int arg0) {
            }

            @Override
            public void onStopped() {

            }

        };

        private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {

            @Override
            public void onAdStarted() {
            }

            @Override
            public void onError(YouTubePlayer.ErrorReason arg0) {

                Log.e("video_size","onError");

                Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
                Date currentLocalTime = cal.getTime();
                DateFormat date = new SimpleDateFormat("HH:mm a");
                date.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
                String localTime = date.format(currentLocalTime);
                SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_WORLD_READABLE|MODE_WORLD_WRITEABLE).edit();
                editor.putString("name", ""+localTime+"video_2");
                editor.commit();

                Intent intent =new Intent(getApplicationContext(),GalleryActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fadeout, R.anim.fadein);

            }
            @Override
            public void onLoaded(String arg0) {
            }

            @Override
            public void onLoading() {

              // Toast.makeText(getApplicationContext(),"OnLOADING",Toast.LENGTH_LONG);
                Log.e("video_size","onLoading");
                try
                {
                    //int flag=0;

                if(!Utils.checkInternetConnection(getApplicationContext()))
                {
                    handler.removeCallbacks(runnable);
                    handler.removeCallbacksAndMessages(null);

                    Intent intent =new Intent(getApplicationContext(),GalleryActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fadeout, R.anim.fadein);
                }

               handler = new Handler();
               runnable = new Runnable() {
                    public void run() {

                        flag++;
                        Log.e("Flag",""+flag);

                        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
                        Date currentLocalTime = cal.getTime();
                        DateFormat date = new SimpleDateFormat("HH:mm a");
                        date.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
                        String localTime = date.format(currentLocalTime);
                        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_WORLD_READABLE|MODE_WORLD_WRITEABLE).edit();
                        editor.putString("name", ""+localTime+"video_3");
                        editor.commit();

                        Intent intent =new Intent(getApplicationContext(),GalleryActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.fadeout, R.anim.fadein);

                    }
                };
                handler.postDelayed(runnable, 10000);
                }catch (Exception e)
                {
                    Intent intent =new Intent(getApplicationContext(),GalleryActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fadeout, R.anim.fadein);
                    e.printStackTrace();
                }
            }

            @Override
            public void onVideoEnded() {

                i=i+1;

                Log.e("onVideoEnded","Video ended"+i);
                if (i< videoList.size())
                {
                    player1.loadVideo(videoList.get(i).getLink());
                    Log.e("video_list","Video ended"+i);
                }
                else
                {
                    Log.e("Video","Video ended");
                    Intent intent =new Intent(getApplicationContext(),GalleryActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fadeout, R.anim.fadein);
                }
            }
            @Override
            public void onVideoStarted() {
                Log.e("video_size","Video onVideoStarted"+i);
            }
        };
 }
