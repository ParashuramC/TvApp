package dnm.com.rmz.Bean;

/**
 * Created by droidev on 26/10/16.
 */
public class Birthday {

    String name;
    String image;
    int duration;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
