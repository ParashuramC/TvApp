package dnm.com.rmz.Bean;

/**
 * Created by droidev on 20/11/16.
 */
public class DayWhether {

    int min;
    int max;
    String Dayname;
    String  image;

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public String getDayname() {
        return Dayname;
    }

    public void setDayname(String dayname) {
        Dayname = dayname;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
