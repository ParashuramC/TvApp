package dnm.com.rmz.Bean;

import java.util.ArrayList;

/**
 * Created by droidev on 27/10/16.
 */
public class Gallery {

    String name;
    String image;
    int duration;
    ArrayList<Gallery> image_list ;

    public ArrayList<Gallery> getImage_list() {
        return image_list;
    }

    public void setImage_list(ArrayList<Gallery> image_list) {
        this.image_list = image_list;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
