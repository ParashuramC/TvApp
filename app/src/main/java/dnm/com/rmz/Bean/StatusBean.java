package dnm.com.rmz.Bean;

/**
 * Created by droid2 on 6/10/16.
 */
public class StatusBean {
    String status;
    int failure_id;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getFailure_id() {
        return failure_id;
    }

    public void setFailure_id(int failure_id) {
        this.failure_id = failure_id;
    }
}
