package dnm.com.rmz.Bean;

/**
 * Created by droidev on 19/11/16.
 */
public class Wheather {

    int curr_wheather;
    String city;
    String bg_image;
    String Image;
    int min;
    int max;
    String wheather_report;
    int d1_min;
    int d1_max;
    String d1_image;
    String d1_name;
    int d2_min;
    int d2_max;
    String d2_image;
    String d2_name;
    int d3_min;
    int d3_max;
    String d3_image;
    String d3_name;
    int d4_min;
    int d4_max;
    String d4_image;
    String d4_name;
    int d5_min;
    int d5_max;
    String d5_image;
    String d5_name;
    int duration;

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getD1_min() {
        return d1_min;
    }

    public void setD1_min(int d1_min) {
        this.d1_min = d1_min;
    }

    public int getD1_max() {
        return d1_max;
    }

    public void setD1_max(int d1_max) {
        this.d1_max = d1_max;
    }

    public String getD1_image() {
        return d1_image;
    }

    public void setD1_image(String d1_image) {
        this.d1_image = d1_image;
    }

    public String getD1_name() {
        return d1_name;
    }

    public void setD1_name(String d1_name) {
        this.d1_name = d1_name;
    }

    public int getD2_min() {
        return d2_min;
    }

    public void setD2_min(int d2_min) {
        this.d2_min = d2_min;
    }

    public int getD2_max() {
        return d2_max;
    }

    public void setD2_max(int d2_max) {
        this.d2_max = d2_max;
    }

    public String getD2_image() {
        return d2_image;
    }

    public void setD2_image(String d2_image) {
        this.d2_image = d2_image;
    }

    public String getD2_name() {
        return d2_name;
    }

    public void setD2_name(String d2_name) {
        this.d2_name = d2_name;
    }

    public int getD3_min() {
        return d3_min;
    }

    public void setD3_min(int d3_min) {
        this.d3_min = d3_min;
    }

    public int getD3_max() {
        return d3_max;
    }

    public void setD3_max(int d3_max) {
        this.d3_max = d3_max;
    }

    public String getD3_image() {
        return d3_image;
    }

    public void setD3_image(String d3_image) {
        this.d3_image = d3_image;
    }

    public String getD3_name() {
        return d3_name;
    }

    public void setD3_name(String d3_name) {
        this.d3_name = d3_name;
    }

    public int getD4_min() {
        return d4_min;
    }

    public void setD4_min(int d4_min) {
        this.d4_min = d4_min;
    }

    public int getD4_max() {
        return d4_max;
    }

    public void setD4_max(int d4_max) {
        this.d4_max = d4_max;
    }

    public String getD4_image() {
        return d4_image;
    }

    public void setD4_image(String d4_image) {
        this.d4_image = d4_image;
    }

    public String getD4_name() {
        return d4_name;
    }

    public void setD4_name(String d4_name) {
        this.d4_name = d4_name;
    }

    public int getD5_min() {
        return d5_min;
    }

    public void setD5_min(int d5_min) {
        this.d5_min = d5_min;
    }

    public int getD5_max() {
        return d5_max;
    }

    public void setD5_max(int d5_max) {
        this.d5_max = d5_max;
    }

    public String getD5_image() {
        return d5_image;
    }

    public void setD5_image(String d5_image) {
        this.d5_image = d5_image;
    }

    public String getD5_name() {
        return d5_name;
    }

    public void setD5_name(String d5_name) {
        this.d5_name = d5_name;
    }

    public String getBg_image() {
        return bg_image;
    }

    public void setBg_image(String bg_image) {
        this.bg_image = bg_image;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public int getCurr_wheather() {
        return curr_wheather;
    }

    public void setCurr_wheather(int curr_wheather) {
        this.curr_wheather = curr_wheather;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public String getWheather_report() {
        return wheather_report;
    }

    public void setWheather_report(String wheather_report) {
        this.wheather_report = wheather_report;
    }
}
