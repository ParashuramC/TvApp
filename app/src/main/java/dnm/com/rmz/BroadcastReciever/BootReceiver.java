package dnm.com.rmz.BroadcastReciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import dnm.com.rmz.Activity.SplashScreenActivity;

public class BootReceiver extends BroadcastReceiver {
    static final String ACTION = "android.intent.action.BOOT_COMPLETED";
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("rmzee","good morning "+intent.getAction());
        if (intent.getAction().equals(ACTION)) {
            Intent myIntent = new Intent(context, SplashScreenActivity.class);
            myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(myIntent);
        }
    }
}