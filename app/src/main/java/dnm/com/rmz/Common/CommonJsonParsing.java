package dnm.com.rmz.Common;

import android.app.Activity;
import android.app.Service;
import android.util.Log;

import com.android.volley.RequestQueue;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;

import dnm.com.rmz.Bean.StatusBean;


/**
 * Created by droid2 on 23/9/16.
 */
public class CommonJsonParsing {
    Activity activity;
    String url;
    Service service;
    RequestQueue requestQueue;
    Service service1;

    public CommonJsonParsing(Activity activity,String url) {
        this.activity = activity;
        this.url = "http://thebay.dnmdigital.com/androidtv/feeds/api/"+url+"/format/json";
       // this.url = "http://dnmdigital.com/projects/androidtv/feeds/index.php/api/"+url+"/format/json";
        //this.url = "http://192.168.0.218/androidtv/feeds/api/"+url+"/format/json";


       // http://192.168.0.218/androidtv/feeds/api/masters/format/json
    }

    public CommonJsonParsing(String url) {

        this.url = "http://thebay.dnmdigital.com/androidtv/feeds/api/"+url+"/format/json";
       // this.url = "http://dnmdigital.com/projects/androidtv/feeds/index.php/api/"+url+"/format/json";
      //  this.url = "http://192.168.0.218/androidtv/feeds/api/"+url+"/format/json";
    }

    public String postJson(final HashMap<String,String> hashMap){
        String charset = "UTF-8";
        HttpURLConnection conn = null;
        DataOutputStream wr;
        StringBuilder result = null;
        URL urlObj;
        JSONObject jObj = null;
        StringBuilder sbParams;
        String paramsString;


        sbParams = new StringBuilder();
        int i = 0;
        for (String key : hashMap.keySet()) {
            try {
                if (i != 0){
                    sbParams.append("&");
                }
                sbParams.append(key).append("=")
                        .append(URLEncoder.encode(hashMap.get(key), charset));

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            i++;
        }


            try {
                urlObj = new URL(url);

                conn = (HttpURLConnection) urlObj.openConnection();

                conn.setDoOutput(true);

                conn.setRequestMethod("POST");

                conn.setRequestProperty("Accept-Charset", charset);

                conn.setReadTimeout(50000);
                conn.setConnectTimeout(55000);

                conn.connect();

                paramsString = sbParams.toString();

                wr = new DataOutputStream(conn.getOutputStream());
                wr.writeBytes(paramsString);
                wr.flush();
                wr.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        try {
            //Receive the response from the server
            InputStream in = new BufferedInputStream(conn.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            result = new StringBuilder();

            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }

            Log.e(" JSON Parser", "result: "+url+""+"\n"+ result.toString());
            in.close();
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        conn.disconnect();


        return result.toString();
    }

    public StatusBean checkStatus(String result){
        JSONObject object = null;
        StatusBean bean = new StatusBean();
        try {
            object = new JSONObject(result);
            for (Iterator<String> itr = object.keys(); itr.hasNext();){
                String test = itr.next();
                if (test.equalsIgnoreCase("Failure")){
                    bean.setStatus("failure");
                    bean.setFailure_id(object.getInt(test));
                }else{
                    bean.setStatus("Success");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return bean;
    }
}
