package dnm.com.rmz.Common;


import android.content.Context;

import java.util.HashMap;


/**
 * Created by HARI on 6/24/2015.
 */
public class SessionManager {

    public static Context context;
    private static HashMap<String,Object> sessionObj;

    public static void SaveData(String key,Object value)
    {
        if (sessionObj==null)
        {
            sessionObj=new HashMap<String,Object>();
        }
        if(sessionObj.get(key)!=null)
        {
            sessionObj.remove(key);
        }
        sessionObj.put(key,value);
    }

    public static Object RetrieveData(String key)
    {
        Object obj=null;
        if(sessionObj==null)
        {
            sessionObj=new HashMap<String,Object>();
        }
        if(sessionObj.get(key)!=null)
        {
            obj=sessionObj.get(key);
        }

        return  obj;
    }

    public static Boolean isKeyAvailable(String key)
    {
        if (sessionObj==null)
        {
            sessionObj=new HashMap<String,Object>();
        }
        if(sessionObj.get(key)!=null)
            return true;
        else
            return  false;
    }
    public static String RemoveData(String key)
    {
        if (sessionObj==null)
        {
            sessionObj=new HashMap<String,Object>();
        }
        if(sessionObj.get(key)!=null)
        {
            sessionObj.remove(key);
        }
        return key;
    }
}

