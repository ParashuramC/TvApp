package dnm.com.rmz.Database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by droidev on 6/2/17.
 */
public class Rmz_Db extends SQLiteOpenHelper {

    public SQLiteDatabase dbSqlite;
    private static String DB_PATH = "/data/data/dnm.com.rmz/databases/";
    private static String DB_NAME = "rmz.sqlite"; //Database name
    private static int DB_VERSION = 1;
    Context context;
    private String TAG = "Rmz_Db";
    public String id = "id";
    public String file_path = "file_path";
    public String status = "status";
    public String rmz_videos = "rmz_videos";


    public Rmz_Db(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;

        String outFileName =context.getDatabasePath(DB_NAME).getPath() ;

        Log.e("Path",outFileName);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @SuppressLint("NewApi")
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        dbSqlite.deleteDatabase(new File(DB_PATH+DB_NAME));
    }


    public void createDatabase() {
        createDB();
    }

    private void createDB() {
        Boolean dbExist = DBExists();
        if (!dbExist){
            this.getWritableDatabase();
            copyDBFromResource();
            Log.e(TAG,"Database copyed from pre-populated DB");
        }else{
            //Log.e(TAG,"Database already exist");
        }
    }

    @NonNull
    private Boolean DBExists() {
        File dbFile = new File(DB_PATH + DB_NAME);
        // Log.d(TAG,"database found "+dbFile.exists());
        return dbFile.exists();
    }

    private void copyDBFromResource() {
        InputStream inputStream = null;
        OutputStream outputStream = null;

        String dbFilePath = context.getDatabasePath(DB_NAME).getPath();

        try {
            inputStream = context.getAssets().open(DB_NAME);
            outputStream = new FileOutputStream(dbFilePath);

            byte[] buffer = new byte[1024];
            int length;
            while ((length = inputStream.read(buffer)) > 0){
                outputStream.write(buffer,0,length);
            }
            outputStream.close();
            inputStream.close();

        } catch (IOException e) {
            throw new Error("Problem copying database from resource file.");
        }
    }
    public void openDataBase(){
        try {
            String myPath = DB_PATH + DB_NAME;
            dbSqlite = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
            dbSqlite.execSQL("PRAGMA automatic_index = off");
            //dbSqlite.beginTransaction();
            Log.e(TAG, "Open database");
            Log.e(TAG, ""+myPath);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    @Override
    public synchronized void close() {
        if (dbSqlite != null){
            dbSqlite.close();
        }
        //Log.e(TAG,"Db is closed");
        super.close();
    }

    public void insert_File_path(int id, String file_path, int status){
        ContentValues values = new ContentValues();
        values.put(this.id,id);
        values.put(this.file_path,file_path);
        values.put(this.status,status);
        dbSqlite.replace(rmz_videos,"",values);
    }

    public boolean deleteTitle(int id1)
    {
        return dbSqlite.delete(rmz_videos,  id+ "=" + id1, null) > 0;
    }

//delete from rmz_videos where id="+id,null)

    public String getFile_path(int id){

        Cursor cursor = dbSqlite.rawQuery("select file_path from rmz_videos where id="+id,null);
        String string = "";
        Log.e("Cursor",""+cursor.getCount());
        if (cursor.moveToFirst()){

            string = cursor.getString(cursor.getColumnIndex("file_path"));
        }
        return string;
    }


    public int getCount_video(){
        int count=0;
        try
        {
            Cursor cursor = dbSqlite.rawQuery(" select * from rmz_videos where status = 1 ",null);
                count = cursor.getCount();
        }catch (Exception e)
        {
            e.printStackTrace();
        }


        return count;
    }

    public int Ids(){
        int count=0;
        try
        {
            Cursor cursor = dbSqlite.rawQuery("select id from rmz_videos",null);
            count =cursor.getCount();

        }catch (Exception e)
        {
            e.printStackTrace();
        }

        return count;
    }

    public  Cursor  getAll_IDs()
    {
        Cursor cursor = dbSqlite.rawQuery("select id from rmz_videos",null);
        return  cursor;
    }

    public int getBoot_value(int id){

        Cursor cursor = dbSqlite.rawQuery("select boot_value from tv_boot_value where id ="+id,null);

        int count = 0;
        if (cursor.moveToFirst()){
            count = cursor.getInt(cursor.getColumnIndex("count"));
        }
        return count;
    }

    public int getBoot_id(int id){

        Cursor cursor = dbSqlite.rawQuery("select boot_id from tv_boot_value where id ="+id,null);
        int id1 = 0;
        if (cursor.moveToFirst()){
            Log.e("Cursor",""+cursor);
            id1 = cursor.getInt(cursor.getColumnIndex("boot_id"));
        }
        return id1;
    }
}
