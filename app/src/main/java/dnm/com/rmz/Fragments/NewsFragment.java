package dnm.com.rmz.Fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import dnm.com.rmz.Bean.News_tech;
import dnm.com.rmz.Common.CommonJsonParsing;
import dnm.com.rmz.Common.SessionManager;
import dnm.com.rmz.R;
import dnm.com.rmz.utilities.CustomImageView;

/**
 * Created by droidev on 11/11/16.
 */
public class NewsFragment extends Fragment {


    TextView tv_title, tv_sub_tittle, tv_description, tv_image_below;
    List<News_tech> tvBeanList;
    CommonJsonParsing commonJsonParsing;
    private String result;
    RelativeLayout relativeLayout;
    CustomImageView image_an;
    Animation animationFadeIn,animationFadeOut;
    Handler handler,handler1;
    Runnable runnable1;
    List<News_tech> flipBoardList;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view=inflater.inflate(R.layout.activity_announcement, container, false);

        tv_title = (TextView)view.findViewById(R.id.an_tv_title);
        tv_sub_tittle = (TextView)view.findViewById(R.id.an_tv_sub_title);
        tv_description = (TextView)view.findViewById(R.id.an_tv_description);
        image_an = (CustomImageView)view.findViewById(R.id.an_image);
        relativeLayout= (RelativeLayout)view.findViewById(R.id.rel);

        tv_title.setText((String) SessionManager.RetrieveData("heading"));
        tv_sub_tittle.setText((String) SessionManager.RetrieveData("title"));
        tv_description.setText((String) SessionManager.RetrieveData("description"));
        image_an.setImageUrl((String) SessionManager.RetrieveData("NewsImage"));

        flipBoardList=new ArrayList<>();
        flipBoardList= (List<News_tech>) SessionManager.RetrieveData("Flip_board");

        animationFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fadein);
        animationFadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fadeout);

        return view;
    }
}
