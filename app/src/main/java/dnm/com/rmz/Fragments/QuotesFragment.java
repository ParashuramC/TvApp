package dnm.com.rmz.Fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dnm.com.rmz.Bean.Quotes;
import dnm.com.rmz.Common.CommonJsonParsing;
import dnm.com.rmz.Common.SessionManager;
import dnm.com.rmz.R;
import dnm.com.rmz.utilities.CustomImageView;


public class QuotesFragment extends Fragment {

    TextView tvquotes,tvtxt;
    Animation animationFadeIn,animationFadeOut;
    Context context;

    int[] layout_id;

    Handler handler;
    Runnable runnable;
    CommonJsonParsing commonJsonParsing;
    private String result;
    List<Quotes> tvquotesList;
    RelativeLayout relativeLayout;

    List<Quotes> quotesList;

    CustomImageView quotesimage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.quotes_fragment, container, false);

        quotesimage = (CustomImageView) view.findViewById(R.id.quotes_images);



        quotesimage.setImageUrl((String) SessionManager.RetrieveData("QuoteImage"));




      /*  quotesList=new ArrayList<>();
        quotesList= (List<Quotes>) SessionManager.RetrieveData("Quotes");


      // tvquotes.setText(quotesList.get(0).getQuotes());

        handler = new Handler();

        Runnable runnable = new Runnable() {
            int i = 0;

            public void run() {

               // tvquotes.setText(quotesList.get(i).getQuotes());
                i++;
                if (i > quotesList.size() - 1) {
                    i = 0;

                }
                handler.postDelayed(this, 5000);
            }
        };
        handler.postDelayed(runnable, 0);


        //   new Tvtask().execute();
        layout_id=new int[]{R.id.quotes_tv_annoucement,R.id.quotes_tv_annoucement};


        animationFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fadein);
        animationFadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fadeout);


        runnable = new Runnable() {
            int i = 0;

            public void run() {

                SessionManager.SaveData("Key","");
                Intent intent=new Intent(getActivity(), BdayActivity.class);
                startActivity(intent);


                handler.postDelayed(this, 15000);

            }
        };
        handler.postDelayed(runnable, 15000);*/

        return view;
    }



    private class Tvtask extends AsyncTask<Void,Void,Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            commonJsonParsing = new CommonJsonParsing(getActivity(),"masters");
        }

        @Override
        protected Void doInBackground(Void... params) {


            HashMap hashMap = new HashMap();
            hashMap.put("date","2016-01-2015");

            result = commonJsonParsing.postJson(hashMap);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            if (result!=null){
                try {

                    JSONObject object = new JSONObject(result);

                    Log.d("Value:==",""+result);

                    JSONArray parentarray = object.getJSONArray("quotes");

                    tvquotesList=new ArrayList<>();

                    for (int i = 0; i < parentarray.length(); i++) {
                        JSONObject finalobject = parentarray.getJSONObject(i);

                        Log.e("finalobject:==",""+finalobject.getString("quotes"));


                        Quotes quotes=new Quotes();
                        tvquotesList.add(quotes);


                    }


                    final Handler handler = new Handler();

                    Runnable runnable = new Runnable() {
                        int i = 0;

                        public void run() {

                           // tvquotes.setText(tvquotesList.get(i).getQuotes());
                            i++;
                            if (i > tvquotesList.size() - 1) {
                                i = 0;

                            }
                            if(i%2==0)
                            {
                                relativeLayout.startAnimation(animationFadeOut);
                            }
                            else
                            {
                                relativeLayout.startAnimation(animationFadeIn);
                            }


                            handler.postDelayed(this, 10000);

                        }
                    };
                    handler.postDelayed(runnable, 0);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }

    }



}
