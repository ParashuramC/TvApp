package dnm.com.rmz.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import dnm.com.rmz.Bean.Wheather;
import dnm.com.rmz.Common.SessionManager;
import dnm.com.rmz.R;
import dnm.com.rmz.utilities.CustomImageView;

/**
 * Created by droidev on 20/11/16.
 */
public class WheatherFragment extends Fragment {

    TextView tv_city_name,tv_degree,tv_min_degree,tv_max_degree,
            tv_wheather_report,tv_week_name1,tv_week_name2,tv_week_name3,
            tv_week_name4,tv_week_name5,tv_d_min_degree1,tv_d_min_degree2,
            tv_d_min_degree3,tv_d_min_degree4,tv_d_min_degree5,tv_d_max_degree1,
            tv_d_max_degree2,tv_d_max_degree3,tv_d_max_degree4,tv_d_max_degree5;

    CustomImageView im_big_image,im_small1,im_small2,im_small3,im_small4,im_small5;
    List<Wheather> whether_list;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view=inflater.inflate(R.layout.fragment_whether, container, false);

        tv_city_name= (TextView)view. findViewById(R.id.w_tv_city_name);
        tv_degree= (TextView) view.findViewById(R.id.w_tv_current_degree);
        /*tv_min_degree= (TextView) findViewById(R.id.w_tv_min_degree);
        tv_max_degree= (TextView) findViewById(R.id.w_tv_max_degree);
        tv_wheather_report= (TextView) findViewById(R.id.w_tv_wheather_report);
        tv_week_name1= (TextView) findViewById(R.id.w_tv_week_name1);
        tv_week_name2= (TextView) findViewById(R.id.w_tv_week_name2);
        tv_week_name3= (TextView) findViewById(R.id.w_tv_week_name3);
        tv_week_name4= (TextView) findViewById(R.id.w_tv_week_name4);
        tv_week_name5= (TextView) findViewById(R.id.w_tv_week_name5);
        tv_d_min_degree1= (TextView) findViewById(R.id.w_tv_d_min_degree1);
        tv_d_min_degree2= (TextView) findViewById(R.id.w_tv_d_min_degree2);
        tv_d_min_degree3= (TextView) findViewById(R.id.w_tv_d_min_degree3);
        tv_d_min_degree4= (TextView) findViewById(R.id.w_tv_d_min_degree4);
        tv_d_min_degree5= (TextView) findViewById(R.id.w_tv_d_min_degree5);
        tv_d_max_degree1= (TextView) findViewById(R.id.w_tv_d_max_degree1);
        tv_d_max_degree2= (TextView) findViewById(R.id.w_tv_d_max_degree2);
        tv_d_max_degree3= (TextView) findViewById(R.id.w_tv_d_max_degree3);
        tv_d_max_degree4= (TextView) findViewById(R.id.w_tv_d_max_degree4);
        tv_d_max_degree5= (TextView) findViewById(R.id.w_tv_d_max_degree5);
        im_big_image= (CustomImageView) findViewById(R.id.w_im_big);
        im_small1= (CustomImageView) findViewById(R.id.w_im_small_image1);
        im_small2= (CustomImageView) findViewById(R.id.w_im_small_image2);
        im_small3= (CustomImageView) findViewById(R.id.w_im_small_image3);
        im_small4= (CustomImageView) findViewById(R.id.w_im_small_image4);
        im_small5= (CustomImageView) findViewById(R.id.w_im_small_image5);
        im_template= (CustomImageView) findViewById(R.id.w_im_template);
*/

        whether_list = (ArrayList<Wheather>) SessionManager.RetrieveData("Final_wh_list");

       // tv_city_name.setText(whether_list.get());



        return view;
    }
}
