package dnm.com.rmz.Service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import dnm.com.rmz.Activity.SplashScreenActivity;
import dnm.com.rmz.BroadcastReciever.MyWakefulReceiver;

/**
 * Created by droidev on 15/3/17.
 */
public class MyIntentService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    public MyIntentService() {
        super("MyIntentService");
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        Log.e("extras",""+extras);
        Intent myIntent = new Intent(MyIntentService.this, SplashScreenActivity.class);
        myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        MyIntentService.this.startActivity(myIntent);
        MyWakefulReceiver.completeWakefulIntent(intent);
    }
}
