package dnm.com.rmz.Service;

import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import dnm.com.rmz.Activity.BdayActivity;
import dnm.com.rmz.Activity.MyApplication;
import dnm.com.rmz.Activity.SplashScreenActivity;
import dnm.com.rmz.Bean.Birthday;
import dnm.com.rmz.Bean.Gallery;
import dnm.com.rmz.Bean.HrAnnoucement;
import dnm.com.rmz.Bean.News_tech;
import dnm.com.rmz.Bean.Quotes;
import dnm.com.rmz.Bean.Video;
import dnm.com.rmz.Bean.Wheather;
import dnm.com.rmz.Common.CommonJsonParsing;
import dnm.com.rmz.Common.SessionManager;

/**
 * Created by droidev on 7/11/16.
 */
public class TVService extends Service {

    CommonJsonParsing commonJsonParsing;
    private String result;
    long diffSeconds00 ;
    long diffMinutes00 ;
    long diffHours00 ;
    long diffDays00 ;
    long diffSeconds01 ;
    long diffMinutes01 ;
    long diffHours01 ;
    long diffDays01 ;
    long diff;
    private long diff1;
    private Intent intent;
    JSONObject object;
    JSONArray parentarray;
    String formattedDate;
    List<Birthday> tvbirthdaylist;
    List<News_tech> news_list;
    ArrayList<Gallery> gallery_list;
    ArrayList final_gallery_list;
    List<Wheather> final_wheather_list;
    List<Birthday> birthday_list;
    private JSONArray parentarray1;
    Handler handler;
    Runnable runnable;
    List<Video> videoList;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        final Handler handler1 = new Handler();

        Runnable runnable1 = new Runnable() {

            public void run() {

                try {

                    new TvAsync().execute();
                    Log.e("Feeds call","2min");

                }catch (Throwable e)
                {
                    e.printStackTrace();
                }

                handler1.postDelayed(this, 60000*60);

            }
        };
        handler1.postDelayed(runnable1, 60000*60);

         handler = new Handler();

         runnable = new Runnable() {

            public void run() {

                try {

                    if(isInBackground()) {
                        restoreApp(); // restore!
                        Log.e("Running_b1","no running background restarting the app");
                    }

                    tvbirthdaylist = (List<Birthday>) SessionManager.RetrieveData("bdaylist");
                    Bdaytimings1();
                    Bdaytimings2();
                    Bdaytimings3();
                    Bdaytimings4();

                }catch (Throwable e)
                {
                    e.printStackTrace();
                }

                handler.postDelayed(this, 5000);

            }
        };
        handler.postDelayed(runnable, 5000);
        try
        {
            //Log.e("Running_out", "no running background restarting the app");
            if (isInBackground()) {
                restoreApp(); // restore!
                Log.e("Running", "no running background restarting the app");
            }
        }catch (Exception e)
        {
            Log.e("Running_b","error in service "+e.getMessage());
            e.printStackTrace();
        }
        return START_STICKY;

    }
    private void restoreApp() {
        // Restart activity
        Intent i = new Intent(TVService.this, SplashScreenActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        TVService.this.startActivity(i);
    }

    private boolean isInBackground() {
        ActivityManager am = (ActivityManager) TVService.this.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
        ComponentName componentInfo = taskInfo.get(0).topActivity;
        return (!TVService.this.getApplicationContext().getPackageName().equals(componentInfo.getPackageName()));
    }
    private void Bdaytimings1() {

        TimeZone tz = TimeZone.getTimeZone("Asia/Kolkata");
        Calendar c = Calendar.getInstance(tz);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate1 = df.format(c.getTime());
        formattedDate=formattedDate1+" "+c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE) +  ":"+c.get(Calendar.SECOND)  ;

        String dateStart = formattedDate;
        String dateStop =  formattedDate1+" "+"10"+":"+"00"+":"+"00";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            diff = d2.getTime() - d1.getTime();

            diffSeconds00 = diff / 1000 % 60;
            diffMinutes00 = diff / (60 * 1000) % 60;
            diffHours00 = diff / (60 * 60 * 1000) % 24;
            diffDays00 = diff / (24 * 60 * 60 * 1000);


        } catch (Throwable e) {
            e.printStackTrace();
        }

      try
      {
        String dateStart1 = formattedDate;
        String dateStop1 = formattedDate1+" "+"10"+":"+"00"+":"+String.valueOf(tvbirthdaylist.get(0).getDuration());

        Date d3 = null;
        Date d4 = null;

        try {
            d3 = format.parse(dateStart1);
            d4 = format.parse(dateStop1);

            diff1 = d4.getTime() - d3.getTime();

            diffSeconds01 = diff1 / 1000 % 60;
            diffMinutes01 = diff1 / (60 * 1000) % 60;
            diffHours01 = diff1 / (60 * 60 * 1000) % 24;
            diffDays01 = diff1 / (24 * 60 * 60 * 1000);

            if(diff<=0 && diff1>=0)
            {
               // stopSelf();

                Log.e("bday1",""+tvbirthdaylist.get(0).getName());
                stopService(new Intent(getApplicationContext(), TVService.class));

                intent =new Intent(getApplicationContext(), BdayActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }
      }catch (Throwable e)
      {
          e.printStackTrace();
      }

    }

    private void Bdaytimings2() {

        TimeZone tz = TimeZone.getTimeZone("Asia/Kolkata");
        Calendar c = Calendar.getInstance(tz);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate1 = df.format(c.getTime());
        formattedDate=formattedDate1+" "+c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE) +  ":"+c.get(Calendar.SECOND)  ;

        String dateStart = formattedDate;
        String dateStop =  formattedDate1+" "+"12"+":"+"00"+":"+"00";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            diff = d2.getTime() - d1.getTime();

            diffSeconds00 = diff / 1000 % 60;
            diffMinutes00 = diff / (60 * 1000) % 60;
            diffHours00 = diff / (60 * 60 * 1000) % 24;
            diffDays00 = diff / (24 * 60 * 60 * 1000);

        } catch (Throwable e) {
            e.printStackTrace();
        }

        try
        {


        String dateStart1 = formattedDate;
        String dateStop1 = formattedDate1+" "+"12"+":"+"00"+":"+String.valueOf(tvbirthdaylist.get(0).getDuration());

        Date d3 = null;
        Date d4 = null;

        try {
            d3 = format.parse(dateStart1);
            d4 = format.parse(dateStop1);

            diff1 = d4.getTime() - d3.getTime();

            diffSeconds01 = diff1 / 1000 % 60;
            diffMinutes01 = diff1 / (60 * 1000) % 60;
            diffHours01 = diff1 / (60 * 60 * 1000) % 24;
            diffDays01 = diff1 / (24 * 60 * 60 * 1000);

            if(diff<=0 && diff1>=0)
            {
                stopService(new Intent(getApplicationContext(), TVService.class));
                intent =new Intent(getApplicationContext(), BdayActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }

        } catch (Throwable e) {
            e.printStackTrace();
        }
        }catch (Throwable e) {
            e.printStackTrace();
        }

    }

    private void Bdaytimings3() {

        TimeZone tz = TimeZone.getTimeZone("Asia/Kolkata");
        Calendar c = Calendar.getInstance(tz);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate1 = df.format(c.getTime());
        formattedDate=formattedDate1+" "+c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE) +  ":"+c.get(Calendar.SECOND)  ;

        String dateStart = formattedDate;
        String dateStop =  formattedDate1+" "+"14"+":"+"00"+":"+"00";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            diff = d2.getTime() - d1.getTime();

            diffSeconds00 = diff / 1000 % 60;
            diffMinutes00 = diff / (60 * 1000) % 60;
            diffHours00 = diff / (60 * 60 * 1000) % 24;
            diffDays00 = diff / (24 * 60 * 60 * 1000);

        } catch (Throwable e) {
            e.printStackTrace();
        }

        try {
        String dateStart1 = formattedDate;
        String dateStop1 = formattedDate1+" "+"14"+":"+"00"+":"+String.valueOf(tvbirthdaylist.get(0).getDuration());

        Date d3 = null;
        Date d4 = null;

        try {
            d3 = format.parse(dateStart1);
            d4 = format.parse(dateStop1);

            diff1 = d4.getTime() - d3.getTime();

            diffSeconds01 = diff1 / 1000 % 60;
            diffMinutes01 = diff1 / (60 * 1000) % 60;
            diffHours01 = diff1 / (60 * 60 * 1000) % 24;
            diffDays01 = diff1 / (24 * 60 * 60 * 1000);

            if(diff<=0 && diff1>=0)
            {
                stopService(new Intent(getApplicationContext(), TVService.class));
                intent =new Intent(getApplicationContext(), BdayActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void Bdaytimings4() {

        TimeZone tz = TimeZone.getTimeZone("Asia/Kolkata");
        Calendar c = Calendar.getInstance(tz);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate1 = df.format(c.getTime());
        formattedDate=formattedDate1+" "+c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE) +  ":"+c.get(Calendar.SECOND)  ;

        String dateStart = formattedDate;
        String dateStop =  formattedDate1+" "+"16"+":"+"00"+":"+"00";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            diff = d2.getTime() - d1.getTime();

            diffSeconds00 = diff / 1000 % 60;
            diffMinutes00 = diff / (60 * 1000) % 60;
            diffHours00 = diff / (60 * 60 * 1000) % 24;
            diffDays00 = diff / (24 * 60 * 60 * 1000);

        } catch (Throwable e) {
            e.printStackTrace();
        }

        try {
        String dateStart1 = formattedDate;
        String dateStop1 = formattedDate1+" "+"16"+":"+"00"+":"+String.valueOf(tvbirthdaylist.get(0).getDuration());

        Date d3 = null;
        Date d4 = null;

        try {
            d3 = format.parse(dateStart1);
            d4 = format.parse(dateStop1);

            diff1 = d4.getTime() - d3.getTime();

            diffSeconds01 = diff1 / 1000 % 60;
            diffMinutes01 = diff1 / (60 * 1000) % 60;
            diffHours01 = diff1 / (60 * 60 * 1000) % 24;
            diffDays01 = diff1 / (24 * 60 * 60 * 1000);

            if(diff<=0 && diff1>=0)
            {
                stopService(new Intent(getApplicationContext(), TVService.class));
                intent =new Intent(getApplicationContext(), BdayActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void clearCache() {
        File[] dir = getApplicationContext().getCacheDir().listFiles();
        if(dir != null){
            for (File f : dir){
                f.delete();
            }
        }
    }


    @Override
    public void onDestroy() {
        try
        {
            handler.removeCallbacks(runnable);

        }catch (Exception e)
        {
            e.printStackTrace();
        }
        super.onDestroy();

    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    private class TvAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try{
                commonJsonParsing = new CommonJsonParsing("masters");

            }catch (Throwable e)
            {
                e.printStackTrace();
            }

        }

        @Override
        protected Void doInBackground(Void... params) {


            HashMap hashMap = new HashMap();
            hashMap.put("date", "2016-01-2015");


            try {
                result = commonJsonParsing.postJson(hashMap);
            }catch (Throwable e)
            {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            if (result != null) {
                try {

                    object = new JSONObject(result);

                    Log.d("Value:==", "" + result);

                    parentarray = object.getJSONArray("news_tech");
                    parentarray1 = object.getJSONArray("quotes");

                    news_list = new ArrayList<>();

                    for (int i = 0; i < parentarray.length();  i++) {

                        JSONObject finalobject1 = parentarray1.getJSONObject(i);
                        News_tech quotes = new News_tech();
                        quotes.setDuration(finalobject1.getInt("duration"));
                        quotes.setImage(finalobject1.getString("image"));
                        news_list.add(quotes);

                        JSONObject finalobject = parentarray.getJSONObject(i);
                        News_tech tvBean = new News_tech();
                        tvBean.setTitle(finalobject.getString("title"));
                        tvBean.setSubtitle(finalobject.getString("sub_title"));
                        tvBean.setDescription(finalobject.getString("description"));
                        tvBean.setImage(finalobject.getString("image"));
                        tvBean.setDuration(finalobject.getInt("duration"));
                        news_list.add(tvBean);

                    }

                    News_tech news_tech =new News_tech();
                    news_list.add(news_tech);

                    SessionManager.SaveData("newlist",news_list);

                    parentarray = object.getJSONArray("birthday");
                    birthday_list = new ArrayList<>();

                    for (int i = 0; i < parentarray.length(); i++) {

                        JSONObject finalobject = parentarray.getJSONObject(i);
                        Birthday birth = new Birthday();
                        birth.setDuration(finalobject.getInt("duration"));
                        birth.setImage(finalobject.getString("image"));
                        birth.setName(finalobject.getString("name"));
                        birthday_list.add(birth);
                    }

                    SessionManager.RemoveData("bdaylist");

                    Birthday birthday =new Birthday();
                    birthday_list.add(birthday);

                    SessionManager.SaveData("bdaylist",birthday_list);



                    parentarray = object.getJSONArray("gallery");
                    gallery_list = new ArrayList<Gallery>();
                    gallery_list.clear();
                    final_gallery_list = new ArrayList<>();
                    final_gallery_list.clear();
                    for (int i=0;i<parentarray.length();i++)
                    {
                        JSONObject jsonobject=parentarray.optJSONObject(i);

                        Log.e("JsonValue",""+jsonobject);
                        JSONArray jsonarry=jsonobject.getJSONArray("image");

                        for (int j=0;j<jsonarry.length();j++)
                        {
                            Gallery gallery=new Gallery();
                            JSONObject obj = jsonarry.optJSONObject(j);
                            gallery.setDuration(obj.getInt("duration"));
                            gallery.setImage(obj.getString("image"));
                            gallery_list.add(gallery);
                        }

                    }

                    Gallery gallery=new Gallery();
                    gallery_list.add(gallery);

                    SessionManager.SaveData("galleryllst",gallery_list);

                    parentarray = object.getJSONArray("weather-details");

                    final_wheather_list = new ArrayList<>();
                    final_wheather_list.clear();

                    for (int i=0;i<parentarray.length();i++)
                    {
                        JSONObject jsonobject=parentarray.optJSONObject(i);

                        Wheather wheather=new Wheather();
                        wheather.setCity(jsonobject.getString("city"));
                        wheather.setWheather_report(jsonobject.getString("weather-report"));
                        wheather.setCurr_wheather(jsonobject.getInt("current-weather"));
                        wheather.setMax(jsonobject.getInt("max"));
                        wheather.setMin(jsonobject.getInt("min"));
                        wheather.setBg_image(jsonobject.getString("bg-image"));
                        wheather.setImage(jsonobject.getString("common-image"));
                        wheather.setD1_min(jsonobject.getInt("d1_min"));
                        wheather.setD2_min(jsonobject.getInt("d2_min"));
                        wheather.setD3_min(jsonobject.getInt("d3_min"));
                        wheather.setD4_min(jsonobject.getInt("d4_min"));
                        wheather.setD5_min(jsonobject.getInt("d5_min"));
                        wheather.setD1_max(jsonobject.getInt("d1_max"));
                        wheather.setD2_max(jsonobject.getInt("d2_max"));
                        wheather.setD3_max(jsonobject.getInt("d3_max"));
                        wheather.setD4_max(jsonobject.getInt("d4_max"));
                        wheather.setD5_max(jsonobject.getInt("d5_max"));
                        wheather.setD1_name(jsonobject.getString("d1_name"));
                        wheather.setD2_name(jsonobject.getString("d2_name"));
                        wheather.setD3_name(jsonobject.getString("d3_name"));
                        wheather.setD4_name(jsonobject.getString("d4_name"));
                        wheather.setD5_name(jsonobject.getString("d5_name"));
                        wheather.setD1_image(jsonobject.getString("d1_image"));
                        wheather.setD2_image(jsonobject.getString("d2_image"));
                        wheather.setD3_image(jsonobject.getString("d3_image"));
                        wheather.setD4_image(jsonobject.getString("d4_image"));
                        wheather.setD5_image(jsonobject.getString("d5_image"));
                        wheather.setDuration(jsonobject.getInt("duration"));
                        Log.e("cityname",jsonobject.getString("city"));

                        final_wheather_list.add(wheather);

                    }

                    Log.e("Size",""+final_wheather_list.size());
                    JSONObject jsonobject=parentarray.optJSONObject(parentarray.length()-1);

                    Wheather wheather=new Wheather();
                    wheather.setCity(jsonobject.getString("city"));
                    wheather.setWheather_report(jsonobject.getString("weather-report"));
                    wheather.setCurr_wheather(jsonobject.getInt("current-weather"));
                    wheather.setMax(jsonobject.getInt("max"));
                    wheather.setMin(jsonobject.getInt("min"));
                    wheather.setBg_image(jsonobject.getString("bg-image"));
                    wheather.setImage(jsonobject.getString("common-image"));
                    wheather.setD1_min(jsonobject.getInt("d1_min"));
                    wheather.setD2_min(jsonobject.getInt("d2_min"));
                    wheather.setD3_min(jsonobject.getInt("d3_min"));
                    wheather.setD4_min(jsonobject.getInt("d4_min"));
                    wheather.setD5_min(jsonobject.getInt("d5_min"));
                    wheather.setD1_max(jsonobject.getInt("d1_max"));
                    wheather.setD2_max(jsonobject.getInt("d2_max"));
                    wheather.setD3_max(jsonobject.getInt("d3_max"));
                    wheather.setD4_max(jsonobject.getInt("d4_max"));
                    wheather.setD5_max(jsonobject.getInt("d5_max"));
                    wheather.setD1_name(jsonobject.getString("d1_name"));
                    wheather.setD2_name(jsonobject.getString("d2_name"));
                    wheather.setD3_name(jsonobject.getString("d3_name"));
                    wheather.setD4_name(jsonobject.getString("d4_name"));
                    wheather.setD5_name(jsonobject.getString("d5_name"));
                    wheather.setD1_image(jsonobject.getString("d1_image"));
                    wheather.setD2_image(jsonobject.getString("d2_image"));
                    wheather.setD3_image(jsonobject.getString("d3_image"));
                    wheather.setD4_image(jsonobject.getString("d4_image"));
                    wheather.setD5_image(jsonobject.getString("d5_image"));
                    Log.e("cityname",jsonobject.getString("city"));

                    final_wheather_list.add(wheather);
                    SessionManager.SaveData("Final_wh_list",final_wheather_list);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }

    }


}
