package dnm.com.rmz.utilities;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class Utils {
	public static String filePath;
	public static void CopyStream(InputStream is, OutputStream os)
	{
		final int buffer_size=1024;
		try
		{
			byte[] bytes=new byte[buffer_size];
			for(;;)
			{
				int count=is.read(bytes, 0, buffer_size);
				if(count==-1)
					break;
				os.write(bytes, 0, count);
			}
		}
		catch(Exception ex){}
	}


	public static String readFileFromAssets(String fileName, Context applicationContext) {
		try {
			InputStream is = applicationContext.getAssets().open(fileName);
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();
			String text = new String(buffer);
			return text;

		} catch (IOException e) {
			System.out.println("exception :"+e.getMessage());
			throw new RuntimeException(e);
		}
	}

	public static void hideSoftKeyboard(Activity activity) {
		InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
	}

	public static String getCurrentDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date date = new Date();
		return dateFormat.format(date);
	}
	public static String getDay(String dateString) {
		Calendar c = Calendar.getInstance();
		Date date;
		String day="";
		int dayOfWeek=0;
		DateFormat format2 = null;
		System.out.println("date "+dateString+day);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		try{
			date = dateFormat.parse(dateString);
			c.setTime(date);
			dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
			format2=new SimpleDateFormat("EEEE");
			String finalDay=format2.format(dayOfWeek);
			System.out.println("Day of Week = " + dayOfWeek+finalDay);

		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return getIntToDay(dayOfWeek);
	}

	private static String getIntToDay(int dayOfWeek) {
		// TODO Auto-generated method stub
		String day="";
		switch (dayOfWeek) {
		case 1:
			day = "Sunday";
			break;
		case 2:
			day = "Monday";
			break;
		case 3:
			day = "Tuesday";
			break;
		case 4:
			day = "Wednesday";
			break;
		case 5:
			day = "Thursday";
			break;
		case 6:
			day = "Friday";
			break;
		case 7:
			day = "Saturday";
			break;


		default:
			break;
		}
		return day;
	}


	public static String getCurrentTime() {
		Calendar cal = Calendar.getInstance();
		cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		return sdf.format(cal.getTime());
	}

	public static JSONObject getJsonData(String urlString, String... prams) {
		try {
			URL url = new URL(String.format(urlString, prams));
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout((int) 30e3);
			int resCode = conn.getResponseCode();
			if (resCode == 200) {
				InputStream ios = conn.getInputStream();
				ByteArrayOutputStream os = new ByteArrayOutputStream();
				byte[] buf = new byte[4096];
				int n;
				while ((n = ios.read(buf)) >= 0)
					os.write(buf, 0, n);
				os.close();
				ios.close();
				byte[] data = os.toByteArray();
				if (data != null) {
					return new JSONObject(new String(data));
				}
			}
		} catch (Exception e) {
			System.out.println("in exception is json object"+e.getMessage());
		}
		return null;
	}

	public static JSONArray getJsonArray(String urlString, String... prams) {
		HttpURLConnection conn = null;
		try {
			URL url = new URL(String.format(urlString, prams));
			conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout((int) 30e3);
			int resCode = conn.getResponseCode();
			if (resCode == 200) {
				InputStream ios = conn.getInputStream();
				ByteArrayOutputStream os = new ByteArrayOutputStream();
				byte[] buf = new byte[4096];
				int count;
				while ((count = ios.read(buf)) >= 0) {
					os.write(buf, 0, count);
				}
				closeOutputStream(os);
				closeInputStream(ios);
				byte[] data = os.toByteArray();
				if (data != null) {
					return new JSONArray(new String(data));
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} finally {
			conn.disconnect();
		}
		return null;
	}

	public static boolean postJsonData(String urlString, String... prams) {
		try {
			URL url = new URL(String.format(urlString, prams));
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout((int) 30e3);
			int resCode = conn.getResponseCode();
			if (resCode == 200) {
				InputStream ios = conn.getInputStream();
				ByteArrayOutputStream os = new ByteArrayOutputStream();
				byte[] buf = new byte[4096];
				int count;
				while ((count = ios.read(buf)) >= 0) {
					os.write(buf, 0, count);
				}
				closeOutputStream(os);
				closeInputStream(ios);
				byte[] data = os.toByteArray();
				if (data != null) {
					return new Boolean(new String(data));
				}
			}
		} catch (Exception e) {
			// do nothing
		}
		return false;
	}

	private static void closeOutputStream(OutputStream outputStream) {
		try {
			outputStream.close();
		} catch (IOException e) {
			// do nothing
		}
	}

	private static void closeInputStream(InputStream inputStream) {
		try {
			inputStream.close();
		} catch (IOException e) {
			// do nothing
		}
	}


	public static int getJsonIntValue(JSONObject jsonObject, String key) {
		try {
			return jsonObject.getInt(key);
		} catch (JSONException e) {
			// do nothing
		}
		return 0;
	}

	public static long getJsonLongValue(JSONObject jsonObject, String key) {
		try {
			return jsonObject.getLong(key);
		} catch (JSONException e) {
			// do nothing
		}
		return 0;



	}

	public static String getJsonStringValue(JSONObject jsonObject, String key) {
		try {
			return jsonObject.getString(key);
		} catch (JSONException e) {
			// do nothing
		}
		return key;



	}

	public static boolean isPropertyEmpty(String property) {
		if (property == null) {
			return true;
		} else {
			if (property.trim().equals("")) {
				return true;
			}
		}
		return false;
	}

	public static boolean isPropertyNotEmpty(String property) {
		return !isPropertyEmpty(property);
	}

	public static String encodeURL(String url) {
		int index = url.lastIndexOf("/") + 1;

		return url.substring(0, index) + URLEncoder.encode(url.substring(index));
	}


	public static void main(String[] arg) {
		System.out.println(encodeURL("/sdf/sfsdf/sdfsdfs/sfs sfds.png"));
	}

	public static boolean checkInternetConnection(Context context) {
		boolean isConnected = false;
		final ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable() && conMgr.getActiveNetworkInfo()
				.isConnected()) {
			isConnected = true;
		}
		return isConnected;
	}

	public static void setMargins (View v, int l, int t, int r, int b) {
		   if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
		       ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
		       p.setMargins(l, t, r, b);
		       v.requestLayout();
		   }
		}


	public static File createImageFile(FragmentActivity activity, int type) throws IOException {

		File mediaFile = null;

		try {
			// Create an image file name
			int CAMERA_CODE = 1;
			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
			String imageFileName = "Mycar" + timeStamp + "_";
		File storageDir = new
				File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)+"");

			if (type == CAMERA_CODE) {
			mediaFile = File.createTempFile(imageFileName, ".jpg", storageDir);
				filePath = mediaFile.getAbsolutePath();


			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mediaFile;
	}

	public static void deleteDirctory()
	{
		File dir=new File(Environment.getExternalStoragePublicDirectory(
				Environment.DIRECTORY_PICTURES)+"/kalyani");
		if(dir.exists() && dir.isDirectory())
		{
			String[] photos=dir.list();
			for(int i=0;i<photos.length;i++)
			{
				String fileName=photos[i];
				if(fileName.endsWith(".jpeg") || fileName.endsWith(".jpg") && fileName.startsWith("temp") || fileName.startsWith("tempp"))
				{
					// new File(dir,photos[i]).delete();
					new File(dir,fileName).delete();
				}
			}
			  dir.delete();
		}
	}

	public static void deleteFile(String phPath) {
		File deletePhto=new File(phPath);
		if(deletePhto.exists())
		{
			deletePhto.delete();
		}

	}

	public  static boolean isFileExit(String filePath)
	{
		File mfile=new File(filePath);
		if(mfile.exists())
		{
			return  true;
		}
		else
		{
			return false;
		}

	}
}